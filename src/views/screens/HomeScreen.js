import React, {useEffect,useState} from 'react';
import {TouchableOpacity,Image,Text,BackHandler,Alert} from 'react-native';
import {View,HStack,useToast,} from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import fonts from '../core-libs/Fonts';
import {useDispatch, useSelector} from 'react-redux';
import DeviceInfo from 'react-native-device-info';
import { onPressLogout } from '../core-libs/authUtils/logOut';


const HomeScreen = (props) =>{ 
  const toast = useToast();
  const dispatch = useDispatch();
  const date2 = useSelector(state => state.UserReducer?.tokenExpiration);
  const userId1 = useSelector(state => state.UserReducer?.userDataLogin);
  const [uniqueId, setUniqueId] = useState('');
const onPressODI = () => {
  props.navigation.navigate("FirstInnings");
}

const date1 = new Date();
const currentDate = new Date(date1);
const tokenExpiration = new Date(date2);
console.log("c",date1)
console.log("date2",tokenExpiration)

const onPressT20I = () => {
  props.navigation.navigate('FirstInningsTwenty');
}

//back button press removing by this function
const handleBackPress = () => {
  // Check if you want to prevent going back in this screen
  const preventBack = true; // Set this to true or false based on your logic

  if (preventBack) {
    // Show an alert to confirm before navigating back (optional)
    Alert.alert(
      'Confirm',
      'Are you sure you want to go back?',
      [
        { text: 'Cancel', onPress: () => {} },
        {
          text: 'Yes',
          onPress: () => {  BackHandler.exitApp();
         //going out of app
          
          },
        },
      ],
      { cancelable: false }
    );

    // Return true to indicate that the back press is handled in this screen
    return true;
  }

  // If preventBack is false, return false to allow the default behavior (going back)
  return false;
};


useEffect(() => {
  //session expiration for login
 
  if(currentDate >= tokenExpiration ){
    toast.show({
      title: 'Session expired',
      status: 'Error',
      description: 'kindly log in again',
    });
   onPressLogout(dispatch, props,userId1);
    console.log("userId1",userId1)
  }

  //device unique id
  DeviceInfo.getUniqueId().then((uniqueId) => {
   
    console.log("serial number : DeviceInfo.getUniqueId()HomeScreen", uniqueId);
    setUniqueId(uniqueId)
  
  });
 //back button handling
  const backHandler = BackHandler.addEventListener('hardwareBackPress', handleBackPress);

  return () => backHandler.remove();
  
}, []);
console.log("uniqqq",uniqueId)
return (
<>


                        
  <View bg='#000000' flex={1}  width= {wp("100%")} >
   

  
    
    <Image

        style={{width:"100%",height:"28%", resizeMode: 'contain',}}
      
    source={require('@Asset/5.png')}
    

  />
    

       
        <View width={wp("100%")} alignItems='center' justifyContent='center' mt={79}>
        <Text style={{fontFamily:fonts.FastTrack,fontSize:20,textAlign:'center',color: '#FFFFFF', opacity: 0.5}}  
        > Match Format</Text>
             <TouchableOpacity onPress= {onPressODI}>
        <View  bg ='#FFFFFF' width={wp("80%")} mt={8} p={3}>
   
    <HStack  width={wp("80%")} alignItems='center' justifyContent='center'>
      <Text  style={{fontFamily:fonts.FastTrack,fontSize:35,textAlign:'center',color:"#000000"}}  >50 OVERS</Text>
      <Image style={{resizeMode:'contain',alignSelf:'center',marginLeft:10,marginTop:4}} source={require('@Asset/cricicon32.png')}/>
</HStack>


</View>
</TouchableOpacity>
<TouchableOpacity 
   onPress= {onPressT20I} 
  >
    
<View  bg ='#FFFFFF' width={wp("80%")} mt={5} p={3} >


<HStack  width={wp("80%")} alignItems='center' justifyContent='center'>
      <Text  style={{fontFamily:fonts.FastTrack,fontSize:35,color:"#000000"}}  >20 OVERS</Text>
      <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/cricicon32.png')}/>
</HStack>
        
 

</View>
</TouchableOpacity>

</View>

<View  position= 'absolute'  bottom={0} width={wp("70%")}  justifyContent='center' alignSelf='center'>

           <Image
        
        style={{width:"100%",resizeMode:'contain',alignSelf:'center',marginBottom:15,marginRight:10}}
      
    source={require('@Asset/sporteclogo.png')}
    

  />
  
  
</View>
  
 
</View>


  </>
  );
  };
  
  export default HomeScreen;



