import React, { Component } from 'react';
import { TouchableOpacity, Image, Animated, ScrollView, Text } from 'react-native';
import { View, useToast, Input, HStack,} from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import { pdfViewRequest, targetRequest } from '../../../store/actions/userActions';
import SlideButton from 'rn-slide-button';
import fonts from '../../core-libs/Fonts';
import DeviceInfo from 'react-native-device-info';
class SecondInnings extends Component {
  constructor() {
    super();
    this.state = {
      ViewArray: [],
      Disable_Button: false,
      status: true,
      team_two_over_bsi_text: '',
      penalty_runs_team_one: 0,
      errorCheck: false,
      errorCheckEmpty: false,
      errorCheck1:false,
      addInterruptionButtonDisable: true,
      view_show: false,
      over_interruption1: [],
      errorCheckInterruption: false,
      errorCheckEmptyInterruption: false,
      wicket_interruption: [],
      reviced_over_interuption: [],
      disableSlide: false,
      uniqueId:"",
      interruption: [{
        id: 0,
        Overs: "",
        Wickets: "",
        RevisedOvers: ""
      }
      ]
    }
    this.animatedValue = new Animated.Value(0);
    this.Array_Value_Index = 0;
    this.ref1 = React.createRef();
    this.over_ref = React.createRef();
    this.wicket_ref = React.createRef();
    this.reviced_over_ref = React.createRef();
    this.prun = React.createRef();
  }
  showToast = () => {
    const toast = useToast();
    toast.show({
      title: "Wrong Data",
      status: "Error",
      //  description: "Thanks for signing up with us.",
    })
  }
  showToast1 = () => {
    const toast = useToast();
    toast.show({
      title: "Session Expired",
      status: "Error",
       description: "Kindly log in again",
    })
  }
  ShowMaxAlert = (EnteredValue) => {
    const { over_match_start } = this.props.route.params;
    const { interruption_first_inng } = this.props.route.params;
    const lastFirstInterruptionItem = interruption_first_inng[interruption_first_inng.length - 1];
    console.log("lat interruption value", lastFirstInterruptionItem);
    console.log("EnteredValue", EnteredValue);
    var regExp = new RegExp(/^\d+(\.[0-5])?$/);
    let first_inning_last_revised_over = lastFirstInterruptionItem.RevisedOvers;
    let entered_valueFlt = parseFloat(EnteredValue);
    if (EnteredValue != "") {
    
      if (first_inning_last_revised_over == 0) {
        
        if (parseFloat(EnteredValue) <= over_match_start) {
          this.setState({
            team_two_over_bsi_text: entered_valueFlt,
            errorCheck: false,
            errorCheckEmpty: false,
            addInterruptionButtonDisable: false,
          })
          if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if (regExp.test(EnteredValue) === false) {
            this.setState({
              team_two_over_bsi_text: "",
              errorCheck: true,
              errorCheckEmpty: true,
              addInterruptionButtonDisable: true,
            })
          }
        }
        }
        else {
          this.setState({
            team_two_over_bsi_text: "",
            errorCheck: true,
            errorCheckEmpty: true,
            addInterruptionButtonDisable: true,
          })
        }
      }
      if (first_inning_last_revised_over != 0) {
        if (parseFloat(EnteredValue) <= first_inning_last_revised_over) {
          this.setState({
            team_two_over_bsi_text: entered_valueFlt,
            errorCheck: false,
            errorCheckEmpty: false,
            addInterruptionButtonDisable: false,
          })
          if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if (regExp.test(EnteredValue) === false) {
            this.setState({
              team_two_over_bsi_text: "",
              errorCheck: true,
              errorCheckEmpty: true,
              addInterruptionButtonDisable: true,
            })
          }
        }
        }
        else {
          this.setState({
            team_two_over_bsi_text: "",
            errorCheck: true,
            errorCheckEmpty: true,
            addInterruptionButtonDisable: false,
          })
        }
      }
    }
    else {
      {
        this.setState({
          team_two_over_bsi_text: "",
          errorCheck: false,
          errorCheckEmpty: true,
          addInterruptionButtonDisable: false,
        })
      };
    }
  };
  ShowEmptyAlert = (EnteredValue) => {
    if (EnteredValue != "") {
      if ((EnteredValue <= 9999)) {
        this.setState({
          errorCheck1:false,
          penalty_runs_team_one: parseFloat(EnteredValue)
        });
      }
      if ( EnteredValue == "-" ||  EnteredValue == "." || EnteredValue == ","  || EnteredValue == " ") {
        this.setState({
          errorCheck1:true,
          penalty_runs_team_one: "",
        });
      }
    }
    else {
      this.setState({
        errorCheck1:true,
        penalty_runs_team_one: "",
        
      });
    }
  };
  // ShowEmptyAlert = (EnteredValue) => {
  //   var regExp = /^\d+(\.[0-5])?$/;
  //   if (EnteredValue != "" ) {
  //     if(EnteredValue <= 9999){
  //     this.setState({ penalty_runs_team_one: EnteredValue }, () => {
  //       this.setState({
  //         errorCheck: false,
  //         errorCheckEmpty: false
  //       })
  //     });
  //   }
  //     if (EnteredValue <=0 ) {
  //       this.setState({ penalty_runs_team_one: '' }, () => {
  //         this.setState({

  //           errorCheckEmpty: true
  //         })
  //       });
  //     }

  //   }


  //   else {
  //     this.setState({
  //       errorCheck: true,
  //       penalty_runs_team_one: ""
  //     });
  //   }
  // };
  ShowInterruptionOverAlert = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed(1);
    var enteredValueString = (EnteredValue).toString()
    var enteredValueFloat = parseFloat(EnteredValue)
    if (key == 0) {
      if (EnteredValue != "") {
        // Do Something
        if ((enteredValueFloat < this.state.team_two_over_bsi_text)) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            })
          })
        }
      
        if (
          (enteredValueFloat >= this.state.team_two_over_bsi_text) ||
          (enteredValueFloat <= 0) || 
          (regExp.test(EnteredValue) == false)
        )
         {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
             
            })
          })
        }
       

      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Overs: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
          })
        })
      }
    }
    if (key > 0) {
      let prev_rev_overs = [];
      let pre_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == (key - 1)) {
          prev_rev_overs.push(item.RevisedOvers);
        }
      });
      let prev_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_overs.push(item.Overs);
        }
      });
      if (EnteredValue != "") {
        if (((enteredValueFloat > pre_overs[0]) || (enteredValueFloat < prev_rev_overs[0]))) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets:"",
            RevisedOvers:"",
          }
          this.setState({
            interruption: [...this.state.interruption]
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            })
          })
        }

        if (
          
            (enteredValueFloat <= pre_overs[0]) || 
            (enteredValueFloat >= prev_rev_overs[0]) ||
            (enteredValueFloat <= 0) || 
            (regExp.test(EnteredValue) == false)
          ) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets:"",
            RevisedOvers:"",
          }
          this.setState({
            interruption: [...this.state.interruption]
          }, () => {
            this.setState({
              errorCheckInterruption: true,
              
            })
          })
        }
      
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Overs: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable:true
          })
        })
      }
    }
  };
  ShowInterruptionOverAlertError = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed(1);
    var enteredValueString = (EnteredValue).toString()
    var enteredValueFloat = parseFloat(EnteredValue)
    if (key == 0) {
      if (EnteredValue != "") {
        // Do Something
        if ((enteredValueFloat < this.state.team_two_over_bsi_text)) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            })
          })
        }
        if (enteredValueFloat >= this.state.team_two_over_bsi_text) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: "" };
            }
            return item
          });

          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
            })
          })
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable:true
            })
          })
        }
        if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if ((regExp.test(EnteredValue) == false)) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return { ...item, id: key.toString(), Overs: "" };
              }
              return item
            });
            this.setState({
              interruption: updatedInturruption
            }, () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              })
            })
          }
        }

      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Overs: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
          })
        })
      }
    }
    if (key > 0) {
      let prev_rev_overs = [];
      let pre_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == (key - 1)) {
          prev_rev_overs.push(item.RevisedOvers);
        }
      });
      let prev_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_overs.push(item.Overs);
        }
      });
      if (EnteredValue != "") {
        if (((enteredValueFloat > pre_overs[0]) || (enteredValueFloat < prev_rev_overs[0]))) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets:"",
            RevisedOvers:"",
          }
          this.setState({
            interruption: [...this.state.interruption]
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            })
          })
        }
        if ((enteredValueFloat <= pre_overs[0]) || (enteredValueFloat >= prev_rev_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
            })
          })
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Overs: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
            })
          })
        }
        if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if ((regExp.test(EnteredValue) == false)) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return { ...item, id: key.toString(), Overs: "" };
              }
              return item
            });
            this.setState({
              interruption: updatedInturruption
            }, () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              })
            })
          }
        }
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Overs: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable:true
          })
        })
      }
    }
  };
  ShowInterruptionWicketsAlert = (EnteredValue, key) => {
    var EnteredValue = parseInt(EnteredValue);
    if (key == 0) {
      if (EnteredValue <= 9 || EnteredValue == 0) {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Wickets: EnteredValue };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption
        }, () => {
          this.setState({
            errorCheckInterruption: false,
            errorCheckEmptyInterruption: false,
          })
        })
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), Wickets: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            addInterruptionButtonDisable: true
          })
        })
      }
    }
    if (key > 0) {
      let pre_wickets = [];
      let prev_wickets = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_wickets.push(item.Wickets);
        }
      });
      if (EnteredValue <= 9 || EnteredValue == 0) {
        if ((EnteredValue >= pre_wickets[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Wickets: EnteredValue };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            })
          })
        }
        if ((EnteredValue < pre_wickets[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Wickets: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption
          }, () => {
            this.setState({
              errorCheckInterruption: true,
           
            })
          })
        }
      }
else {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), Wickets: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true
            })
          })
        }
    }
  };
  ShowInterruptionRevisedOversAlert = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    // var enteredValueInt = enteredValueFloat.toFixed(1);
    var enteredValueString = (EnteredValue).toString();
    var enteredValueFloat = parseFloat(EnteredValue);
    // const {team_two_over_bsi_text} = this.props.route.params; 
    if (key == 0) {
      let curr_overs = [];
      let current_overIntr = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
        // return item
      });
      if (EnteredValue != "") {
        //if less than over when match starts
        if ((enteredValueFloat < this.state.team_two_over_bsi_text) || (enteredValueFloat > curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: false
            })
          })
        }
        if (
          (enteredValueFloat >= this.state.team_two_over_bsi_text) || 
          (enteredValueFloat <= curr_overs[0])
          || (enteredValueFloat<=0) || 
          (regExp.test(EnteredValue) == false)
          
          ) 
          {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption
          }, () => {
            this.setState({
              errorCheckInterruption: true,
             // errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: true
            })
          })
        }


      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), RevisedOvers: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable: true,
          })
        })
      }
    }
    if (key > 0) {
      let pre_revised_overs = [];
      let curr_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_revised_overs.push(item.RevisedOvers);
        }
        // return item
      });
      let current_overs = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
        //return item
      });
      
      if (EnteredValue != "") {
        if ((enteredValueFloat < pre_revised_overs[0]) || (enteredValueFloat > curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item,id: key.toString(),  RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({ interruption: updatedInturruption }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: false
            })
          });
        }
        if (
          (enteredValueFloat >= pre_revised_overs[0]) 
          || (enteredValueFloat <= curr_overs[0])
          || (enteredValueFloat<=0) || 
      (regExp.test(EnteredValue) == false)
          ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item,id: key.toString(),  RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({ interruption: updatedInturruption }, () => {
            this.setState({
              errorCheckInterruption: true,
              
              addInterruptionButtonDisable: true
            })
          });
        }
  
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), RevisedOvers: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable: true,
          })
        })
      }
    }
  };
  ShowInterruptionRevisedOversAlertError = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    // var enteredValueInt = enteredValueFloat.toFixed(1);
    var enteredValueString = (EnteredValue).toString();
    var enteredValueFloat = parseFloat(EnteredValue);
    // const {team_two_over_bsi_text} = this.props.route.params; 
    if (key == 0) {
      let curr_overs = [];
      let current_overIntr = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
        // return item
      });
      if (EnteredValue != "") {
        //if less than over when match starts
        if ((enteredValueFloat < this.state.team_two_over_bsi_text) || (enteredValueFloat > curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption
          }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: false
            })
          })
        }
        if ((enteredValueFloat >= this.state.team_two_over_bsi_text) || (enteredValueFloat <= curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true,
            })
          })
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            })
          })
        }
        if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if ((regExp.test(EnteredValue) == false)) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return { ...item, id: key.toString(), RevisedOvers: "" };
              }
              return item
            });
            this.setState({
              interruption: updatedInturruption
            }, () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              })
            })
          }
        }
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), RevisedOvers: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable: true,
          })
        })
      }
    }
    if (key > 0) {
      let pre_revised_overs = [];
      let curr_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_revised_overs.push(item.RevisedOvers);
        }
        // return item
      });
      let current_overs = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
        //return item
      });

      if (EnteredValue != "") {
        if ((enteredValueFloat < pre_revised_overs[0]) || (enteredValueFloat > curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item,id: key.toString(),  RevisedOvers: enteredValueString };
            }
            return item
          });
          this.setState({ interruption: updatedInturruption }, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: false
            })
          });
        }
        if ((enteredValueFloat >= pre_revised_overs[0]) || (enteredValueFloat <= curr_overs[0])) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true,
            })
          })
        }

        if (enteredValueFloat <= 0 ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return { ...item, id: key.toString(), RevisedOvers: "" };
            }
            return item
          });
          this.setState({
            interruption: updatedInturruption,
          }, () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            })
          })
        }
        if (EnteredValue == Math.floor(EnteredValue) ==false) {
          if ((regExp.test(EnteredValue) == false)) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return { ...item, id: key.toString(), RevisedOvers: "" };
              }
              return item
            });
            this.setState({
              interruption: updatedInturruption
            }, () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              })
            })
          }
        }
      }
      else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return { ...item, id: key.toString(), RevisedOvers: "" };
          }
          return item
        });
        this.setState({
          interruption: updatedInturruption,
        }, () => {
          this.setState({
            errorCheckInterruption: true,
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable: true,
          })
        })
      }
    }
  };
  calculatePage = () => {
    const { over_match_start } = this.props.route.params;
    const { runs_at_first_innis } = this.props.route.params;
    const { interruption_first_inng } = this.props.route.params;
    // const {team_two_over_bsi_text} = this.props.route.params; 
    const interruption_second_inng = this.state.interruption;
    const lastFirstInterruptionItem = interruption_first_inng[interruption_first_inng.length - 1];
    console.log("lat interruption value22", lastFirstInterruptionItem.RevisedOvers);
    let first_inning_last_revised_over = lastFirstInterruptionItem.RevisedOvers;
    console.log("lat  value22", first_inning_last_revised_over);
    const lastSecondInterruptionItem = interruption_second_inng[interruption_second_inng.length - 1];
    console.log("lat interruption value33", lastSecondInterruptionItem.RevisedOvers);
    let second_inning_last_revised_over = lastSecondInterruptionItem.RevisedOvers;
    console.log("lat  value33", second_inning_last_revised_over);
    console.log("overs_55", over_match_start)
    console.log("runs_55", runs_at_first_innis)
    console.log("interruption_55", interruption_first_inng.length)
    console.log("interruption_66", interruption_first_inng[interruption_first_inng.length - 1])
    // console.log("team_55",team_two_over_bsi_text)
    // console.log("team_550",team_two_over_bsi_text)
    console.log("team2", this.state.team_two_over_bsi_text)
    console.log("penalty", this.state.penalty_runs_team_one)
    console.log("uniqueIduniqueId", this.state.uniqueId)
    if ((first_inning_last_revised_over != 0) && (second_inning_last_revised_over != 0)) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: interruption_first_inng,
        // Fint:[{id:"0",Overs:"11.5",Wickets:"5",RevisedOvers:"41.0" }],
        FRf: runs_at_first_innis,
        penaltyRuns: this.state.penalty_runs_team_one,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: interruption_second_inng,
        uniqueId:this.state.uniqueId
        // //  Sint:[{ id:"0",Overs:"9.3",Wickets:"1", RevisedOvers:"25.0" }]
      }
       
    }
    if (first_inning_last_revised_over != 0 && second_inning_last_revised_over == 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: interruption_first_inng,
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: [],
        uniqueId:this.state.uniqueId
      }
    }
    if (first_inning_last_revised_over == 0 && second_inning_last_revised_over != 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: [],
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: interruption_second_inng,
        uniqueId:this.state.uniqueId
      }
    }
    if (first_inning_last_revised_over == 0 && second_inning_last_revised_over == 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: [],
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: [],
        uniqueId:this.state.uniqueId
      }
    }

    console.log("data for result#############",data)
    this.props.target_request(data, (error, success) => {
      if (error) {
        console.log("error of ", error.response.status);
        if(error.response.status===401){
          console.log("error of #############", error.response.status);
          this.showToast1
          this.props.navigation.navigate("Login")
          
        }
        // else{ this.showToast;
        //   console.log("error of ", error.response.status);}
       
      }
      if (success) {
        if (success.status == 200) {
           this.pdfView()
       
        }
      }
      // console.log("success of ", success.data.TargetRuns);
      console.log("ddd ", data);
    });
  }
  pdfView = () => {
    const { over_match_start } = this.props.route.params;
    const { runs_at_first_innis } = this.props.route.params;
    const { interruption_first_inng } = this.props.route.params;
    // const {team_two_over_bsi_text} = this.props.route.params; 
    const interruption_second_inng = this.state.interruption;
    const lastFirstInterruptionItem = interruption_first_inng[interruption_first_inng.length - 1];
    let first_inning_last_revised_over = lastFirstInterruptionItem.RevisedOvers;
    const lastSecondInterruptionItem = interruption_second_inng[interruption_second_inng.length - 1];
    let second_inning_last_revised_over = lastSecondInterruptionItem.RevisedOvers;
    if ((first_inning_last_revised_over != 0) && (second_inning_last_revised_over != 0)) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: interruption_first_inng,
        // Fint:[{id:"0",Overs:"11.5",Wickets:"5",RevisedOvers:"41.0" }],
        FRf: runs_at_first_innis,
        penaltyRuns: this.state.penalty_runs_team_one,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: interruption_second_inng,
        uniqueId:this.state.uniqueId
        // //  Sint:[{ id:"0",Overs:"9.3",Wickets:"1", RevisedOvers:"25.0" }]
      }
      // // console.log("data for result",data)
    }
    if (first_inning_last_revised_over != 0 && second_inning_last_revised_over == 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: interruption_first_inng,
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: [],
        uniqueId:this.state.uniqueId
      }
    }
    if (first_inning_last_revised_over == 0 && second_inning_last_revised_over != 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: [],
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: interruption_second_inng,
        uniqueId:this.state.uniqueId
      }
    }
    if (first_inning_last_revised_over == 0 && second_inning_last_revised_over == 0) {
      var data = {
        FOs: over_match_start.toString(),
        Fint: [],
        penaltyRuns: this.state.penalty_runs_team_one,
        FRf: runs_at_first_innis,
        SOs: this.state.team_two_over_bsi_text.toString(),
        Sint: [],
        uniqueId:this.state.uniqueId
      }
    }
    this.props.pdf_View_request(data, (error, success) => {
      if (error) {
        if(error.response.status===401){
          this.props.navigation.navigate("Login")
          this.showToast1
        }
        else{ this.showToast;
          console.log("error of ", error.response.status);}
       
      }
      if (success) {
        //  console.log("pdf_View ",pdf_View);
        if (success.status == 200) {
          // this.props.navigation.navigate("HomeScreen")
          this.props.navigation.navigate('ResultPage', {
            over_match_start: over_match_start,
            runs_at_first_innis: runs_at_first_innis,
            interruption_first_inng: interruption_first_inng,
            team_two_over_bsi_text: this.state.team_two_over_bsi_text,
            penalty_run: this.state.penalty_runs_team_one,
            interruption_second_inng: this.state.interruption,
            target_score: this.props.target_score,
            pdf_View: this.props.pdf_View,
          })
        }
      }
    })
  }
  onPressreset = () => {
    this.Array_Value_Index = 0;
    this.setState({
      status: true,
      addInterruptionButtonDisable: true,
      team_two_over_bsi_text: "",
      penalty_runs_team_one: "",
      ViewArray: [],
      interruption: [{
        id: 0,
        Overs: "",
        Wickets: "",
        RevisedOvers: ""
      }
      ]
    })
  }
  terminator = (id) => {
    console.log("ARRAY", this.Array_Value_Index)
    if (this.Array_Value_Index == 0) {
      let newViewArray = [...this.state.ViewArray];
      let newInterruptions = [...this.state.interruption];
      let findIdx = this.state.interruption.findIndex((inter) => inter.id == id);
      newInterruptions.splice(findIdx);
      newViewArray.splice(findIdx);
      console.log("new in", newInterruptions);
      console.log("new in findIdx", findIdx);
      console.log("new in ViewAraay", this.state.ViewArray);
      this.Array_Value_Index = 0;
      this.setState({
        ViewArray: newViewArray,
        status: true,
        interruption: [{ id: 0, Overs: "", Wickets: "", RevisedOvers: "" }],
        addInterruptionButtonDisable: false, errorCheckEmptyInterruption: false,
        errorCheckInterruption: false,
        penalty_runs_team_one: "",
      });
    }
    else {
      let newViewArray = [...this.state.ViewArray];
      let newInterruptions = [...this.state.interruption];
      let findIdx = this.state.interruption.findIndex((inter) => inter.id == id);
      newViewArray.splice(findIdx);
      console.log("new in", newInterruptions);
      console.log("new in findIdx122", findIdx);
      if (findIdx === -1) {
        this.Array_Value_Index = this.Array_Value_Index - 1;
        this.setState({
          ViewArray: newViewArray,
          interruption: newInterruptions,
          addInterruptionButtonDisable: false, errorCheckEmptyInterruption: false,
          errorCheckInterruption: false,
        });
      }
      else {
        newInterruptions.splice(findIdx);
        this.Array_Value_Index = this.Array_Value_Index - 1;
        this.setState({
          ViewArray: newViewArray,
          interruption: newInterruptions,
          addInterruptionButtonDisable: false, errorCheckEmptyInterruption: false,
          errorCheckInterruption: false,
        });
      }
    }
  }
  Add_New_View_Function = () => {
    this.animatedValue.setValue(0);
    let New_Added_View_Value = { Array_Value_Index: this.Array_Value_Index + 1 }
    this.setState({ addInterruptionButtonDisable: true, ViewArray: [...this.state.ViewArray, New_Added_View_Value], status: false }, () => {
      Animated.timing(
        this.animatedValue,
        {
          toValue: 1,
          duration: 400,
          useNativeDriver: true
        }
      ).start(() => {
        this.Array_Value_Index = this.Array_Value_Index + 1;
        this.setState({ Disable_Button: false });
        this.setState({ status: false });
        this.prun.current.focus();
      });
    });
  }
  componentDidMount() {
    DeviceInfo.getUniqueId().then((uniqueId) => {
      console.log("serial number : DeviceInfo.getUniqueId()SECONDINNINGS", uniqueId);
      this.setState({ uniqueId });
    });
  }
  render() {
    console.log("sunil", this.state.interruption)
    const AnimationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });
    let Render_Animated_View = this.state.ViewArray.map((item, key) => {
      this.Array_Value_Index = key

      return (
        <View
          bg='#FFFFFF'
          alignSelf='center' justifyContent='center'
          mt={5} width={wp("90%")}
          key={key}
        // style = {[ styles.Animated_View_Style, { opacity: this.animatedValue, transform: [{ translateY: AnimationValue }] }]}
        >
          <View bg='#000000' >
            <HStack width={wp("90%")}>
              <Text style={{
                fontFamily: fonts.ProximaNova,
                fontSize: 16,
                color: '#FFFFFF',
                opacity: 0.5,
                fontWeight: 'bold',
                width: wp("50%")
              }}
              >Interruption {key + 1}</Text>
              {(this.state.errorCheckEmptyInterruption) && (key == this.state.ViewArray.length - 1) ? (<View alignSelf='flex-end'>
                <Text style={{ fontWeight: 'bold', fontFamily: fonts.ProximaNova, color: '#ff0000', fontSize: 14 }}
                >Value cannot be empty</Text>
              </View>) : (this.state.errorCheckInterruption) && (key == this.state.ViewArray.length - 1) ? (<View alignSelf='flex-end'>
                <Text style={{ fontWeight: 'bold', fontFamily: fonts.ProximaNova, color: '#ff0000', fontSize: 14 }}
                >Check the data entered</Text>
              </View>) : null
              }
            </HStack>
            <HStack mt={2} bg='#FFFFFF' width={wp("90%")} alignItems='center' justifyContent='center'>
              <View mt={2} >
                <Text style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000', fontSize: 14, alignSelf: 'center',
                }}>Overs</Text>
                <Input
                  width={wp("22%")} p={1} m={1} borderColor='#D3D3D3' color='#000000' fontSize='14' textAlign='center'
                  borderWidth={1}
                  onChangeText={(EnteredValue) => this.ShowInterruptionOverAlert(EnteredValue, key)}
                  onEndEditing={(value)=> this.ShowInterruptionOverAlertError(value.nativeEvent.text,key)}
                  value={this.state.interruption[key]?.Overs}
                  ref={this.over_ref}
                  keyboardType="number-pad"
                  onSubmitEditing={() =>
                    this.wicket_ref.current.focus()}
                  maxLength={4}
                  // placeholder='Overs'
                  placeholderTextColor='#000000'
                />
              </View>
              <View mt={2} >
                <Text style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000', fontSize: 14, alignSelf: 'center',
                }}>Wickets</Text>
                <Input width={wp("22%")} borderWidth={1} m={1} borderColor='#D3D3D3' color='#000000' fontSize='14' textAlign='center' p={1}
                  onChangeText={(EnteredValue) => this.ShowInterruptionWicketsAlert(EnteredValue, key)}
                 
                  value={this.state.interruption[key]?.Wickets}
                  ref={this.wicket_ref}
                  keyboardType="number-pad"
                  //  placeholder='Wickets'
                  placeholderTextColor='#000000'
                  onSubmitEditing={() =>
                    this.reviced_over_ref.current.focus()}
                  maxLength={3} />
              </View>
              <View mt={2} >
                <Text style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000', fontSize: 14, alignSelf: 'center',
                }}>Revised Overs</Text>
                <Input width={wp("22%")} m={1} p={1} borderColor='#D3D3D3' color='#000000' fontSize='14' textAlign='center' borderWidth={1}
                  ref={this.reviced_over_ref}
                  keyboardType="number-pad"
                  onEndEditing={(value)=> this.ShowInterruptionRevisedOversAlertError(value.nativeEvent.text,key)}
                  onChangeText={(EnteredValue) => this.ShowInterruptionRevisedOversAlert(EnteredValue, key)}
                  value={this.state.interruption[key]?.RevisedOvers}
                  //  placeholder='Revised Ovrs'
                  placeholderTextColor='#000000'
                  maxLength={4} />
              </View>
              {(key == this.state.ViewArray.length - 1) ?
                <TouchableOpacity onPress={() => { this.terminator(key) }}>
                  <Image
                    // style={{marginTop:130,marginRight:35}}
                    source={require('@Asset/delete.png')}
                  />
                </TouchableOpacity>
                : null}
            </HStack>
          </View>
        </View>
      );
      // }
    });
    return (
      <>
        <View bg='#000000' flex={1} >
          <Image
            style={{ width: "100%", height: "28%", resizeMode: 'contain', }}
            source={require('@Asset/3.png')}
          />
          <Text style={{fontFamily:fonts.FastTrack,fontSize:20,textAlign:'center',color: '#FFFFFF', opacity: 0.5}}  
        > 50 overs Format</Text>
          <TouchableOpacity onPress={() => { this.onPressreset() }}>
            <View width={wp("24%")} mt={5} mr={5} borderWidth={2} borderColor='#FFFFFF' opacity={0.6} alignSelf='flex-end' alignItems='center' p={0.5}>
              <HStack>
                <Text style={{ fontWeight: 'bold', fontFamily: fonts.ProximaNova, fontSize: 18, textAlign: 'center', alignSelf: 'center', color: '#FFFFFF', opacity: 0.6 }} >Reset</Text>
                <Image style={{ marginLeft: 5, alignSelf: 'center' }} source={require('@Asset/reset.png')} />
              </HStack>
            </View>
          </TouchableOpacity>
          <ScrollView style={{ flexGrow: 1, marginBottom: '15%', marginTop: '5%' }}>
            <HStack alignSelf='center' justifyContent='center' width={wp("90%")} bg='#FFFFFF' p={4}>

              <Text style={{
                fontFamily: fonts.ProximaNova,
                color: '#000000', fontSize: 16, alignSelf: 'center', width: wp("55%")
              }}>Overs when 2nd Innings starts</Text>
              <Input ml={2}
                textAlign='center'
                alignSelf='center'
                width={wp("22%")}
                placeholderTextColor='#000000'
                placeholder='Enter Overs'
                fontSize='14'
                p={1}
                borderColor='#D3D3D3'
                color='#000000'
                borderWidth='1'
                //  borderRadius='10'
                keyboardType="number-pad"
                maxLength={4}
                onChangeText={EnteredValue => this.ShowMaxAlert(EnteredValue)}
                value={this.state.team_two_over_bsi_text}
              />
            </HStack>
            {this.state.errorCheck ? (<View>
              <Text style={{ color: '#ff0000', fontWeight: 'bold', fontFamily: fonts.ProximaNova, alignSelf: 'center', fontSize: 14, marginRight: 2 }}
              >Overs cannot be empty</Text>
            </View>) :
              this.state.errorCheckEmpty ? (<View>
                <Text style={{ color: '#ff0000', fontWeight: 'bold', fontFamily: fonts.ProximaNova, alignSelf: 'center', fontSize: 14, marginRight: 2 }}>Maximum overs exceeded</Text>
              </View>) : null
            }
            <View >
              {
                (this.state.status == false) ?
                  <HStack
                    alignSelf='center' justifyContent='center' width={wp("90%")} mt={5} bg='#FFFFFF' p={4}>
                    <Text style={{
                      fontSize: 16, color: '#000000',
                      fontFamily: fonts.ProximaNova,
                      m: '2',
                      width: wp("52%"),
                      textAlign: 'center',
                      alignSelf: 'center'
                    }}
                    >Penalty to fielding side during 2nd Innings</Text>
                    <Input ml={2}
                      textAlign='center'
                      alignSelf='center'
                      width={wp("25%")}
                      placeholder='Penalty Runs'
                      p={1}
                      placeholderTextColor='#000000'
                      borderColor='#D3D3D3'
                      color='#000000'
                      fontSize='14'
                      keyboardType="number-pad"
                      maxLength={3}
                      onChangeText={EnteredValue => this.ShowEmptyAlert(EnteredValue)}
                      value={this.state.penalty_runs_team_one}
                      ref={this.prun}
                      onSubmitEditing={() =>
                        this.over_ref.current.focus()}
                    />


                 
                  </HStack> : null}
                  {this.state.errorCheck1 ? (<View>
              <Text style={{ color: '#ff0000', fontWeight: 'bold', fontFamily: fonts.ProximaNova, alignSelf: 'center', fontSize: 14, marginRight: 2 }}
              >Check entered run</Text>
            </View>) :null}
              {
                Render_Animated_View
              }
            </View>
            <TouchableOpacity
              disabled={this.state.addInterruptionButtonDisable}
              state={this.state.status}
              onPress={
                this.Add_New_View_Function
              }
            >
              <View alignSelf='center' justifyContent='center' width={wp("70%")} borderWidth={2} mt={5} p={2} mb={25}
                borderColor={(this.state.addInterruptionButtonDisable) ? '#AEAEAE' : '#FFFFFF'}>
                <HStack alignItems='center' justifyContent='center'  >
                  <Text style={{ fontFamily: fonts.ProximaNova, fontSize: 20, textAlign: 'center', color: (this.state.addInterruptionButtonDisable) ? '#AEAEAE' : '#FFFFFF' }}  >Add interruption</Text>
                  <Image style={{ resizeMode: 'contain', marginLeft: 10, marginTop: 4 }} source={require('@Asset/arrow-right-thin.png')} />
                </HStack>
              </View>
            </TouchableOpacity>
          </ScrollView>
          <View position='absolute' bottom={0} width={wp("100%")} justifyContent='center' alignItems='center'>
            {((this.state.team_two_over_bsi_text > 0) && (this.state.errorCheckInterruption === false) && (this.state.addInterruptionButtonDisable === false || this.state.disableSlide === true))
              ?
              <SlideButton title="Calculate"
                titleStyle={{ fontSize: 18 }}
                containerStyle={{
                  backgroundColor: '#000000', borderColor: '#FFFFFF', borderWidth: 2
                }}
                underlayStyle={{ backgroundColor: '#D3D3D3' }}
                alignSelf='center'
                icon={<Image source={require('@Asset/chevron-double-right.png')} />}
                width={wp("80%")}
                onReachedToEnd={() => { this.calculatePage() }}>
              </SlideButton>
              :
              <SlideButton
                containerStyle={{ backgroundColor: '#000000', borderColor: '#AEAEAE', borderWidth: 2 }}
                autoReset
                title="Calculate"
                //titleStyle={{fontSize:18}}
                alignSelf='center'
                position='absolute'
                bottom={0}
                underlayStyle={{ backgroundColor: '#D3D3D3' }}
                disabled={true}
                icon={<Image source={require('@Asset/chevron-double-right.png')} />}

                marginBottom='5'
                marginLeft='5'
                marginRight='5'
                width={wp("80%")}
              >
              </SlideButton>
            }
          </View>
        </View>
      </>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    target_request: (data, callback) => dispatch(targetRequest(data, callback)),
    pdf_View_request: (data, callback) => dispatch(pdfViewRequest(data, callback)),
  };
};
const mapStateToProps = (state) => {
  return {
    // call this state when required in the ui using this.props.currentUser
    target_score: state.UserReducer.target,
    pdf_View: state.UserReducer.pdf,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SecondInnings);