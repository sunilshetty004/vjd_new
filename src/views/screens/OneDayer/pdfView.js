import React from 'react';
import { View, Button } from 'react-native';
//import PDFView from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';

const pdfView = (props) => {
  const {pdf_View_Twenty} = route.params;
  const pdfUrl = pdf_View_Twenty.ballByball;
  const source = { uri: pdfUrl, cache: true };

  const downloadPDF = async () => {
    const response = await RNFetchBlob.config({
      fileCache: true,
    }).fetch('GET', pdfUrl);

    // You can use response.path() to get the local path to the downloaded PDF
  };

  return (
    <View>
      <PDFView
        fadeInDuration={250.0}
        source={source}
        onLoad={() => console.log('PDF Loaded')}
      />
      <Button title="Download PDF" onPress={downloadPDF} />
    </View>
  );
};

export default pdfView;
