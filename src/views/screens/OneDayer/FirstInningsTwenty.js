import React, {Component} from 'react';
import {Animated, ScrollView, Image, Text} from 'react-native';
import {View, Input, HStack} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SlideButton from 'rn-slide-button';
import fonts from '../../core-libs/Fonts';
class FirstInningsTwenty extends Component {
  constructor() {
    super();
    this.state = {
      ViewArray: [],
      Disable_Button: false,
      status: true,
      errorCheck1: false,
      errorCheckEmpty1: false,
      errorCheck2: false,
      errorCheckEmpty2: false,
      overs_match_start: 0,
      runs_made_end_first_innings: '',
      match_name: '',
      addInterruptionButtonDisable: true,
      noInterruptionButtonDisable: false,
      addInterruptionViewDisable: false,
      doneButtondisable: false,
      doneButtondisable2: true,
      renderedInterruptionDisable: false,
      deleteButtonDisable: false,
      showRunsView: false,
      showRunsView1: false,
      slideButtonDisable: true,
      terminate: false,
      status_termnate: false,
      status_ro: [],
      oversInt: '',
      EnteredValue: '',
      enteredHash: false,
      wicketInt: [{id: '0', Wicketsint: '0'}],
      revisedOversInt: [],
      over_interruption1: '',
      wicket_interruption: '',
      reviced_over_interuption: [],
      errorCheckInterruption: false,
      errorCheckEmptyInterruption: false,
      disableSlide: false,
      status_text: [],
      key1: 0,
      interruption: [
        {
          id: 0,
          Overs: '',
          Wickets: '',
          RevisedOvers: '',
        },
      ],
    };

    this.animatedValue = new Animated.Value(0);
    (this.Narray = []), (this.Array_Value_Index = 0);
    this.interruptionIndex = 0;
    this.ref1 = React.createRef();
    this.over_ref = React.createRef();
    this.wicket_ref = React.createRef();
    this.reviced_over_ref = React.createRef();
    this.terminate_ref = React.createRef();
  }
  ShowMaxAlert = EnteredValue => {
    var regExp = RegExp(/^\d+(\.[1-5])?$/);
    if (EnteredValue != '') {
      if (EnteredValue <= 20) {
        this.setState({
          errorCheck1: false,
          overs_match_start: parseFloat(EnteredValue),
          errorCheckEmpty1: false,
          addInterruptionButtonDisable: false,
        });
      }
      if ((EnteredValue == Math.floor(EnteredValue)) == false) {
        if (regExp.test(EnteredValue) === false) {
          this.setState({
            errorCheck1: true,
            overs_match_start: '',
            // errorCheckEmpty1: true,
          });
        }
      }

      if (EnteredValue > 20 || EnteredValue <= 0) {
        this.setState({
          errorCheck1: true,
          overs_match_start: '',
          errorCheckEmpty1: false,
          addInterruptionButtonDisable: true,
        });
      }
    } else {
      this.setState({
        errorCheckEmpty1: true,
        errorCheck1: false,
        overs_match_start: '',
        addInterruptionButtonDisable: true,
      });
    }
  };
  ShowEmptyAlert = EnteredValue => {
    if (EnteredValue != '') {
      if (EnteredValue <= 9999) {
        this.setState({
          errorCheck2: false,
          errorCheckEmpty2: false,
          // addInterruptionButtonDisable: false,
          runs_made_end_first_innings: parseFloat(EnteredValue),
        });
      }
      if (
        EnteredValue > 9999 ||
        EnteredValue <= 0 ||
        this.state.errorCheck1 == true ||
        this.state.errorCheckEmpty1 == true
      ) {
        this.setState({
          errorCheck2: true,
          // addInterruptionButtonDisable: true,
          runs_made_end_first_innings: '',
        });
      }
    } else {
      this.setState({
        errorCheckEmpty2: true,
        errorCheck2: true,
        runs_made_end_first_innings: '',
        //addInterruptionButtonDisable: true
      });
    }
  };
  submit_button = () => {
    {
      this.props.navigation.navigate('SecondInningsTwenty', {
        over_match_start: this.state.overs_match_start,
        runs_at_first_innis: this.state.runs_made_end_first_innings,
        interruption_first_inng: this.state.interruption,
      });
    }
  };

  ShowInterruptionOverAlertError = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed();
    var enteredValueFloat = parseFloat(EnteredValue);
    var enteredValueString = EnteredValue.toString();

    console.log('ENTERERDEDED', EnteredValue);
    if (key == 0) {
      if (EnteredValue != '') {
        //checking validation condition if entered value is less tha overs at the begining
        if (enteredValueFloat < this.state.overs_match_start) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: enteredValueString};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }

        if (enteredValueFloat >= this.state.overs_match_start) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              console.log('Sunil');
              return {...item, id: key.toString(), Overs: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }
        if ((EnteredValue == Math.floor(EnteredValue)) == false) {
          if (regExp.test(EnteredValue) == false) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return {...item, id: key.toString(), Overs: ''};
              }
              return item;
            });
            this.setState(
              {
                interruption: updatedInturruption,
              },
              () => {
                this.setState({
                  errorCheckInterruption: true,
                  addInterruptionButtonDisable: true,
                });
              },
            );
          }
        }

        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckEmptyInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            console.log('Keshav');
            return {...item, id: key.toString(), Overs: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }

    if (key > 0) {
      let prev_rev_overs = [];
      let pre_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          prev_rev_overs.push(item.RevisedOvers);
        }
      });
      let prev_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_overs.push(item.Overs);
        }
      });
      if (EnteredValue != '') {
        if (
          enteredValueFloat > pre_overs[0] ||
          enteredValueFloat < prev_rev_overs[0]
        ) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets: '',
            RevisedOvers: '',
          };
          this.setState(
            {
              interruption: [...this.state.interruption],
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }
        // if ((enteredValueFloat== pre_overs[0]) || (enteredValueFloat== prev_rev_overs[0])) {
        //   let updatedInturruption = this.state.interruption.map(item => {
        //     if (item.id == key) {
        //       return { ...item, id: key.toString(), Overs: "" };
        //     }
        //     return item
        //   });
        //   this.setState({ interruption: updatedInturruption }, () => {
        //     this.setState({
        //       errorCheckInterruption: true,
        //       addInterruptionButtonDisable: true,
        //     })
        //   })
        // }
        if (
          enteredValueFloat <= pre_overs[0] ||
          enteredValueFloat >= prev_rev_overs[0]
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: ''};
            }
            return item;
          });
          this.setState({interruption: updatedInturruption}, () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true,
            });
          });
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckEmptyInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }
        if ((EnteredValue == Math.floor(EnteredValue)) == false) {
          if (regExp.test(EnteredValue) == false) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return {...item, id: key.toString(), Overs: ''};
              }
              return item;
            });
            this.setState(
              {
                interruption: updatedInturruption,
              },
              () => {
                this.setState({
                  errorCheckInterruption: true,
                  addInterruptionButtonDisable: true,
                });
              },
            );
          }
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), Overs: ''};
          }
          return item;
        });
        this.setState({interruption: updatedInturruption}, () => {
          this.setState({
            errorCheckEmptyInterruption: true,
            addInterruptionButtonDisable: true,
          });
        });
      }
    }
  };

  ShowInterruptionOverAlert = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed();
    var enteredValueFloat = parseFloat(EnteredValue);
    var enteredValueString = EnteredValue.toString();

    console.log('ENTERERDEDED', EnteredValue);

    if (key == 0) {
      if (EnteredValue != '') {
        //checking validation condition if entered value is less tha overs at the begining
        if (enteredValueFloat < this.state.overs_match_start) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: enteredValueString};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }

        if (
          enteredValueFloat >= this.state.overs_match_start ||
          enteredValueFloat <= 0 ||
          regExp.test(EnteredValue) == false
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Overs: enteredValueString};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            console.log('Keshav');
            return {...item, id: key.toString(), Overs: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }

    if (key > 0) {
      let prev_rev_overs = [];
      let pre_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          prev_rev_overs.push(item.RevisedOvers);
        }
      });
      let prev_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_overs.push(item.Overs);
        }
      });

      if (EnteredValue != '') {
        if (
          enteredValueFloat > pre_overs[0] ||
          enteredValueFloat < prev_rev_overs[0]
        ) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets: '',
            RevisedOvers: '',
          };
          this.setState(
            {
              interruption: [...this.state.interruption],
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }
        if (
          enteredValueFloat <= pre_overs[0] ||
          enteredValueFloat >= prev_rev_overs[0] ||
          enteredValueFloat <= 0 ||
          regExp.test(EnteredValue) == false
        ) {
          this.state.interruption[key] = {
            id: key.toString(),
            Overs: enteredValueString,
            Wickets: '',
            RevisedOvers: '',
          };
          this.setState(
            {
              interruption: [...this.state.interruption],
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            console.log('Keshav');
            return {...item, id: key.toString(), Overs: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }
  };
  ShowInterruptionWicketsAlert = (EnteredValue, key) => {
    var EnteredValue = parseInt(EnteredValue);

    if (key == 0) {
      if (EnteredValue <= 9 || EnteredValue == 0) {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), Wickets: EnteredValue};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            });
          },
        );
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), Wickets: EnteredValue};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }

    if (key > 0) {
      let pre_wickets = [];
      let prev_wickets = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_wickets.push(item.Wickets);
        }
      });
      if (EnteredValue <= 9 || EnteredValue == 0) {
        if (EnteredValue >= pre_wickets[0]) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Wickets: EnteredValue};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }
        if (EnteredValue < pre_wickets[0]) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), Wickets: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), Wickets: EnteredValue};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }
  };
  //   ShowInterruptionWicketsAlertError = (EnteredValue, key) => {
  //     var EnteredValue = parseInt(EnteredValue);

  //     if (key == 0) {
  //       if (EnteredValue <= 9 || EnteredValue == 0) {
  //         let updatedInturruption = this.state.interruption.map(item => {
  //           if (item.id == key) {
  //             return { ...item, id: key.toString(), Wickets: EnteredValue };
  //           }
  //           return item
  //         });
  //         this.setState({
  //           interruption: updatedInturruption
  //         }, () => {
  //           this.setState({
  //             errorCheckInterruption: false,
  //             errorCheckEmptyInterruption: false,
  //           })
  //         })
  //     }
  //       else {
  //         let updatedInturruption = this.state.interruption.map(item => {
  //           if (item.id == key) {
  //             return { ...item, id: key.toString(), Wickets: "" };
  //           }
  //           return item
  //         });
  //         this.setState({
  //           interruption: updatedInturruption,
  //         }, () => {
  //           this.setState({
  //             errorCheckInterruption: true,
  //             addInterruptionButtonDisable: true
  //           })
  //         })
  //       }
  //     }

  //     if (key > 0) {
  //       let pre_wickets = [];
  //       let prev_wickets = this.state.interruption.map(item => {
  //         if (item.id == key - 1) {
  //           pre_wickets.push(item.Wickets);
  //         }
  //       });
  //       if (EnteredValue <= 9 || EnteredValue == 0) {
  //         if ((EnteredValue >= pre_wickets[0])) {
  //           let updatedInturruption = this.state.interruption.map(item => {
  //             if (item.id == key) {
  //               return { ...item, id: key.toString(), Wickets: EnteredValue };
  //             }
  //             return item
  //           });
  //           this.setState({
  //             interruption: updatedInturruption
  //           }, () => {
  //             this.setState({
  //               errorCheckInterruption: false,
  //               errorCheckEmptyInterruption: false,
  //             })
  //           })
  //         }
  //       }
  // else {
  //           let updatedInturruption = this.state.interruption.map(item => {
  //             if (item.id == key) {
  //               return { ...item, id: key.toString(), Wickets: "" };
  //             }
  //             return item
  //           });
  //           this.setState({
  //             interruption: updatedInturruption,
  //           }, () => {
  //             this.setState({
  //               errorCheckInterruption: true,
  //               addInterruptionButtonDisable: true
  //             })
  //           })
  //         }
  //     }
  //   };
  ShowInterruptionRevisedOversAlert = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed(1);
    var enteredValueString = EnteredValue;
    var enteredValueFloat = parseFloat(EnteredValue);

    if (key == 0) {
      let curr_overs = [];
      let current_overIntr = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
      });
      if (EnteredValue != '') {
        //if less than over when match starts
        if (
          enteredValueFloat < this.state.overs_match_start ||
          enteredValueFloat > parseFloat(curr_overs[0])
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });

          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
              });
            },
          );
        }

        if (
          enteredValueFloat >= this.state.overs_match_start ||
          enteredValueFloat <= parseFloat(curr_overs[0]) ||
          enteredValueFloat <= 0 ||
          regExp.test(EnteredValue) == false
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });

          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
              });
            },
          );
        }
        if (EnteredValue === '#') {
          let terminated_overs = [];
          let terminated_overIntr = this.state.interruption.map(item => {
            if (item.id == key) {
              terminated_overs.push(item.Overs);
            }
          });
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: '#'};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                disableSlide: true,
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
                addInterruptionButtonDisable: true,
                doneButtondisable2: false,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), RevisedOvers: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
              //  doneButtondisable2:false
            });
          },
        );
      }
    }

    if (key > 0) {
      let pre_revised_overs = [];
      let curr_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_revised_overs.push(item.RevisedOvers);
        }
      });
      let current_overs = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
      });

      if (EnteredValue != '') {
        if (
          enteredValueFloat < pre_revised_overs[0] ||
          enteredValueFloat > curr_overs[0]
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });
          this.setState({interruption: updatedInturruption}, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
            });
          });
        }

        if (
          enteredValueFloat >= pre_revised_overs[0] ||
          enteredValueFloat <= curr_overs[0] ||
          enteredValueFloat <= 0 ||
          regExp.test(EnteredValue) == false
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });
          this.setState({interruption: updatedInturruption}, () => {
            this.setState({
              errorCheckInterruption: true,
            });
          });
        }
        if (EnteredValue === '#') {
          let terminated_overs = [];
          let terminated_overIntr = this.state.interruption.map(item => {
            if (item.id == key) {
              terminated_overs.push(item.Overs);
            }
          });
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: '#'};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                disableSlide: true,
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
                addInterruptionButtonDisable: true,
                doneButtondisable2: false,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), RevisedOvers: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }
  };
  ShowInterruptionRevisedOversAlertError = (EnteredValue, key) => {
    var regExp = /^\d+(\.[0-5])?$/;
    var enteredValueInt = parseFloat(EnteredValue).toFixed(1);
    var enteredValueString = EnteredValue;
    var enteredValueFloat = parseFloat(EnteredValue);

    if (key == 0) {
      let curr_overs = [];
      let current_overIntr = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
      });
      if (EnteredValue != '') {
        console.log('typeof(this.state.curr_overs[0])', typeof curr_overs[0]);
        console.log('Entere', EnteredValue);
        console.log(
          'this.state.overs_match_start',
          this.state.overs_match_start,
        );
        console.log('this.state.curr_overs[0]', curr_overs[0]);
        //if less than over when match starts
        if (
          enteredValueFloat < this.state.overs_match_start ||
          enteredValueFloat > parseFloat(curr_overs[0])
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });

          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
                addInterruptionButtonDisable: false,
                doneButtondisable2: false,
              });
            },
          );
        }

        if (
          enteredValueFloat >= this.state.overs_match_start ||
          enteredValueFloat <= parseFloat(curr_overs[0])
        ) {
          let updatedInturruption = [...this.state.interruption];
          let updatedInturruptionCpy = updatedInturruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: ''};
            }

            return item;
          });

          this.setState(
            {
              interruption: updatedInturruptionCpy,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: ''};
            }
            return item;
          });

          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckEmptyInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }

        if ((EnteredValue == Math.floor(EnteredValue)) == false) {
          if (regExp.test(EnteredValue) == false) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return {...item, id: key.toString(), RevisedOvers: ''};
              }
              return item;
            });
            this.setState(
              {
                interruption: updatedInturruption,
              },
              () => {
                this.setState({
                  errorCheckInterruption: true,
                  addInterruptionButtonDisable: true,
                });
              },
            );
          }
        }

        if (EnteredValue === '#') {
          let terminated_overs = [];
          let terminated_overIntr = this.state.interruption.map(item => {
            if (item.id == key) {
              terminated_overs.push(item.Overs);
            }
          });
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: terminated_overs[key],
              };
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                disableSlide: true,
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
                addInterruptionButtonDisable: true,
                doneButtondisable2: false,
                enteredHash: true,
                addInterruptionViewDisable: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), RevisedOvers: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }

    if (key > 0) {
      let pre_revised_overs = [];
      let curr_overs = [];
      let prev_revised_overs = this.state.interruption.map(item => {
        if (item.id == key - 1) {
          pre_revised_overs.push(item.RevisedOvers);
        }
      });
      let current_overs = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overs.push(item.Overs);
        }
      });

      if (EnteredValue != '') {
        if (
          enteredValueFloat < pre_revised_overs[0] ||
          enteredValueFloat > curr_overs[0]
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {
                ...item,
                id: key.toString(),
                RevisedOvers: enteredValueString,
              };
            }
            return item;
          });
          this.setState({interruption: updatedInturruption}, () => {
            this.setState({
              errorCheckInterruption: false,
              errorCheckEmptyInterruption: false,
              addInterruptionButtonDisable: false,
            });
          });
        }
        if (
          enteredValueFloat >= pre_revised_overs[0] ||
          enteredValueFloat <= curr_overs[0]
        ) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }
        if (enteredValueFloat <= 0) {
          let updatedInturruption = this.state.interruption.map(item => {
            if (item.id == key) {
              return {...item, id: key.toString(), RevisedOvers: ''};
            }
            return item;
          });
          this.setState(
            {
              interruption: updatedInturruption,
            },
            () => {
              this.setState({
                errorCheckEmptyInterruption: true,
                addInterruptionButtonDisable: true,
              });
            },
          );
        }

        if ((EnteredValue == Math.floor(EnteredValue)) == false) {
          if (regExp.test(EnteredValue) == false) {
            let updatedInturruption = this.state.interruption.map(item => {
              if (item.id == key) {
                return {...item, id: key.toString(), RevisedOvers: ''};
              }
              return item;
            });
            this.setState(
              {
                interruption: updatedInturruption,
              },
              () => {
                this.setState({
                  errorCheckInterruption: true,
                  addInterruptionButtonDisable: true,
                });
              },
            );
          }
        }

        if (EnteredValue === '#') {
          let updatedInturruptionCpy = [...this.state.interruption];
          if (updatedInturruptionCpy[key].id == key) {
            updatedInturruptionCpy[key].RevisedOvers =
              updatedInturruptionCpy[key].Overs;
          }
          this.setState(
            {
              interruption: updatedInturruptionCpy,
            },
            () => {
              this.setState({
                enteredHash: true,
                errorCheckInterruption: false,
                errorCheckEmptyInterruption: false,
                addInterruptionButtonDisable: true,
                disableSlide: true,
                addInterruptionViewDisable: true,
              });
            },
          );
        }
      } else {
        let updatedInturruption = this.state.interruption.map(item => {
          if (item.id == key) {
            return {...item, id: key.toString(), RevisedOvers: ''};
          }
          return item;
        });
        this.setState(
          {
            interruption: updatedInturruption,
          },
          () => {
            this.setState({
              errorCheckInterruption: true,
              errorCheckEmptyInterruption: true,
              addInterruptionButtonDisable: true,
            });
          },
        );
      }
    }
  };

  terminator = id => {
    if (this.Array_Value_Index == 0) {
      let newViewArray = [...this.state.ViewArray];
      let newInterruptions = [...this.state.interruption];
      let findIdx = this.state.interruption.findIndex(inter => inter.id == id);
      newInterruptions.splice(findIdx);
      newViewArray.splice(findIdx);
      this.Array_Value_Index = 0;
      this.setState({
        ViewArray: newViewArray,
        interruption: [{id: '0', Overs: '', Wickets: '', RevisedOvers: ''}],
        addInterruptionButtonDisable: false,
        errorCheckEmptyInterruption: false,
        errorCheckInterruption: false,
        enteredHash: false,
        doneButtondisable: false,
        noInterruptionButtonDisable: false,
      });
    } else {
      let newViewArray = [...this.state.ViewArray];
      let newInterruptions = [...this.state.interruption];
      let findIdx = this.state.interruption.findIndex(inter => inter.id == id);
      newViewArray.splice(findIdx);
      console.log('new in', newInterruptions);
      console.log('new in findIdx122', findIdx);
      if (findIdx === -1) {
        this.Array_Value_Index = this.Array_Value_Index - 1;
        this.setState({
          ViewArray: newViewArray,
          interruption: newInterruptions,
          addInterruptionButtonDisable: false,
          errorCheckEmptyInterruption: false,
          errorCheckInterruption: false,
          enteredHash: false,
        });
      } else {
        newInterruptions.splice(findIdx);
        this.Array_Value_Index = this.Array_Value_Index - 1;
        this.setState({
          ViewArray: newViewArray,
          interruption: newInterruptions,
          addInterruptionButtonDisable: false,
          errorCheckEmptyInterruption: false,
          errorCheckInterruption: false,
          enteredHash: false,
        });
      }
    }
  };
  onPressreset = () => {
    this.Array_Value_Index = 0;
    this.setState({
      doneButtondisable2: true,
      addInterruptionButtonDisable: true,
      enteredHash: false,
      overs_match_start: '',
      runs_made_end_first_innings: '',
      noInterruptionButtonDisable: false,
      addInterruptionViewDisable: false,
      doneButtondisable: false,
      renderedInterruptionDisable: false,
      deleteButtonDisable: false,
      showRunsView: false,
      showRunsView1: false,
      ViewArray: [],
      interruption: [
        {
          id: '0',
          Overs: '',
          Wickets: '',
          RevisedOvers: '',
        },
      ],
    });
  };
  Add_New_View_Function = () => {
    this.animatedValue.setValue(0);
    let New_Added_View_Value = {Array_Value_Index: this.Array_Value_Index + 1};
    console.log(
      'interruption array value added or not',
      this.state.interruption,
    );
    console.log('interruption ARrray', this.Array_Value_Index);
    this.setState(
      {
        addInterruptionButtonDisable: true,
        ViewArray: [...this.state.ViewArray, New_Added_View_Value],
        status: false,
        terminate: true,
        noInterruptionButtonDisable: true,
        doneButtondisable: true,
      },
      () => {
        Animated.timing(this.animatedValue, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }).start(() => {
          this.Array_Value_Index = this.Array_Value_Index + 1;
          this.setState({status: false, terminate: true});
          this.over_ref.current.focus();
        });
      },
    );
  };
  noInterruption = () => {
    this.setState({
      showRunsView: true,
      noInterruptionButtonDisable: true,
      addInterruptionViewDisable: true,
      doneButtondisable: false,
    });
  };

  onPressDone = () => {
    this.setState({
      showRunsView1: true,
      deleteButtonDisable: true,
      noInterruptionButtonDisable: true,
      addInterruptionViewDisable: true,
      doneButtondisable: false,
    });
  };

  render() {
    const AnimationValue = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-59, 0],
    });
    let Render_Animated_View = this.state.ViewArray.map((item, key) => {
      this.Array_Value_Index = key;
      let index;
      let curr_overss = [];
      let current_overs = this.state.interruption.map(item => {
        if (item.id == key) {
          curr_overss.push(item.Overs);
        }
      });
      return (
        <View
          alignSelf="center"
          justifyContent="center"
          mt={5}
          width={wp('90%')}
          key={key}>
          {console.log('key', key)}
          {console.log('ARRAY', this.Array_Value_Index)}
          {console.log('interruption', this.state.interruption)}
          {console.log('over', this.state.overs_match_start)}
          {console.log('runs', this.state.runs_made_end_first_innings)}
          <View bg="#000000">
            <HStack width={wp('90%')}>
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  fontSize: 16,
                  color: '#FFFFFF',
                  opacity: 0.5,
                  marginRight: 5,
                  width: wp('50%'),
                }}>
                Interruption {key + 1}
              </Text>
              {this.state.errorCheckEmptyInterruption &&
              key == this.state.ViewArray.length - 1 ? (
                <View alignSelf="flex-end">
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontFamily: fonts.ProximaNova,
                      color: '#ff0000',
                      fontSize: 14,
                    }}>
                    Value cannot be empty
                  </Text>
                </View>
              ) : this.state.errorCheckInterruption &&
                key == this.state.ViewArray.length - 1 ? (
                <View>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontFamily: fonts.ProximaNova,
                      color: '#ff0000',
                      fontSize: 14,
                    }}>
                    Check the data entered
                  </Text>
                </View>
              ) : index === this.state.ViewArray.length - 1 ? null : null}
            </HStack>
          </View>
          <HStack
            mt={2}
            bg="#FFFFFF"
            width={wp('90%')}
            alignItems="center"
            justifyContent="center">
            <View mt={2}>
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000',
                  fontSize: 14,
                  alignSelf: 'center',
                }}>
                Overs
              </Text>
              <Input
                width={wp('22%')}
                borderColor="#D3D3D3"
                fontSize="14"
                textAlign="center"
                m={1}
                p={1}
                variant="underlined"
                onChangeText={EnteredValue => {
                  this.ShowInterruptionOverAlert(EnteredValue, key);
                }}
                onEndEditing={value =>
                  this.ShowInterruptionOverAlertError(
                    value.nativeEvent.text,
                    key,
                  )
                }
                value={this.state.interruption[key]?.Overs}
                ref={this.over_ref}
                keyboardType="number-pad"
                onSubmitEditing={() => this.wicket_ref.current.focus()}
                maxLength={4}
                placeholderTextColor="#000000"
                color="#000000"
                borderWidth={2}
              />
            </View>
            <View mt={2}>
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000',
                  fontSize: 14,
                  alignSelf: 'center',
                }}>
                Wickets
              </Text>
              <Input
                width={wp('22%')}
                borderColor="#D3D3D3"
                borderWidth="2"
                m={1}
                textAlign="center"
                color="#000000"
                fontSize="14"
                p={1}
                onChangeText={EnteredValue =>
                  this.ShowInterruptionWicketsAlert(EnteredValue, key)
                }
                //  onEndEditing={(value)=> this.ShowInterruptionWicketsAlertError(value.nativeEvent.text,key)}
                value={this.state.interruption[key]?.Wickets}
                ref={this.wicket_ref}
                variant="underlined"
                keyboardType="number-pad"
                onSubmitEditing={() => this.reviced_over_ref.current.focus()}
                maxLength={1}
                placeholderTextColor="#000000"
              />
            </View>
            <View mt={2}>
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000',
                  fontSize: 14,
                  alignSelf: 'center',
                }}>
                Revised Overs
              </Text>
              <Input
                width={wp('22%')}
                m={1}
                p={1}
                borderColor="#D3D3D3"
                borderWidth="2"
                textAlign="center"
                variant="underlined"
                ref={this.reviced_over_ref}
                keyboardType="phone-pad"
                onChangeText={EnteredValue =>
                  this.ShowInterruptionRevisedOversAlert(EnteredValue, key)
                }
                onEndEditing={value =>
                  this.ShowInterruptionRevisedOversAlertError(
                    value.nativeEvent.text,
                    key,
                  )
                }
                // value={this.state.interruption[key]?.RevisedOvers}
                value={
                  this.state.enteredHash == false
                    ? this.state.interruption[key]?.RevisedOvers
                    : '#'
                }
                maxLength={4}
                editable={true}
                fontSize={14}
                placeholderTextColor="#000000"
                color="#000000"
              />
            </View>
            {key == this.state.ViewArray.length - 1 &&
            this.state.deleteButtonDisable == false ? (
              <TouchableOpacity
                onPress={() => {
                  this.terminator(key);
                }}>
                <Image source={require('@Asset/delete.png')} />
              </TouchableOpacity>
            ) : index === this.state.ViewArray.length - 1 ? null : null}
          </HStack>
          {key == this.state.ViewArray.length - 1 &&
          this.state.enteredHash == false ? (
            <View alignSelf="center">
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#ff9005',
                  fontSize: 18,
                  alignSelf: 'center',
                  textAlign: 'center',
                  width: wp('90%'),
                }}>
                Enter '#' to terminate the Innings
              </Text>
            </View>
          ) : index === this.state.ViewArray.length - 1 ? null : null}
        </View>
      );
      // }
    });
    return (
      <>
        <View bg="#000000" flex={1}>
          <Image
            style={{width: '100%', height: '28%', resizeMode: 'contain'}}
            source={require('@Asset/2.png')}
          />
          <Text
            style={{
              fontFamily: fonts.FastTrack,
              fontSize: 20,
              textAlign: 'center',
              color: '#FFFFFF',
              opacity: 0.5,
            }}>
            {' '}
            20 overs Format
          </Text>
          <TouchableOpacity
            onPress={() => {
              this.onPressreset();
            }}>
            <View
              width={wp('24%')}
              mt={5}
              mr={5}
              borderWidth={2}
              borderColor="#FFFFFF"
              opacity={0.6}
              alignSelf="flex-end"
              alignItems="center"
              p={0.5}>
              <HStack>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontFamily: fonts.ProximaNova,
                    fontSize: 18,
                    textAlign: 'center',
                    alignSelf: 'center',
                    color: '#FFFFFF',
                    opacity: 0.6,
                  }}>
                  Reset
                </Text>
                <Image
                  style={{marginLeft: 5, alignSelf: 'center'}}
                  source={require('@Asset/reset.png')}
                />
              </HStack>
            </View>
          </TouchableOpacity>
          <ScrollView
            style={{flexGrow: 1, marginBottom: '15%', marginTop: '5%'}}>
            {console.log('ARRAY1', this.Array_Value_Index)}
            {console.log('interruption1', this.state.interruption)}
            {console.log('over1', this.state.overs_match_start)}
            {console.log('runs1', this.state.runs_made_end_first_innings)}
            <HStack
              alignSelf="center"
              justifyContent="center"
              width={wp('90%')}
              bg="#FFFFFF"
              p={4}>
              <Text
                style={{
                  fontFamily: fonts.ProximaNova,
                  color: '#000000',
                  fontSize: 16,
                  alignSelf: 'center',
                  width: wp('52%'),
                }}>
                Overs when match starts
              </Text>
              <Input
                width={wp('25%')}
                p={1}
                ml={2}
                // height={ hp("5%")}
                textAlign="center"
                placeholder="Enter Overs"
                placeholderTextColor="#000000"
                fontSize="14"
                borderColor="#D3D3D3"
                borderWidth="1"
                color="#000000"
                keyboardType="number-pad"
                maxLength={4}
                onChangeText={EnteredValue => this.ShowMaxAlert(EnteredValue)}
                value={this.state.overs_match_start}
                // onSubmitEditing={() =>
                //   this.ref1.current.focus()}
              />
            </HStack>
            {this.state.errorCheckEmpty1 ? (
              <View>
                <Text
                  style={{
                    color: '#ff0000',
                    fontWeight: 'bold',
                    fontFamily: fonts.ProximaNova,
                    alignSelf: 'center',
                    fontSize: 14,
                    marginRight: 2,
                  }}>
                  Overs cannot be empty
                </Text>
              </View>
            ) : this.state.errorCheck1 ? (
              <View>
                <Text
                  style={{
                    color: '#ff0000',
                    fontWeight: 'bold',
                    fontFamily: fonts.ProximaNova,
                    alignSelf: 'center',
                    fontSize: 14,
                    marginRight: 2,
                  }}>
                  Maximum overs exceeded
                </Text>
              </View>
            ) : null}
            {this.state.showRunsView == true ? (
              <HStack
                alignSelf="center"
                justifyContent="center"
                width={wp('90%')}
                bg="#FFFFFF"
                p={4}
                mt={5}>
                <Text
                  style={{
                    fontFamily: fonts.ProximaNova,
                    color: '#000000',
                    fontSize: 16,
                    alignSelf: 'center',
                    width: wp('50%'),
                  }}>
                  Score at the end of 1st Innings
                </Text>
                <Input
                  ml={2}
                  textAlign="center"
                  width={wp('25%')}
                  // height={ hp("5%")}
                  alignSelf="center"
                  justifyContent="center"
                  fontSize="14"
                  p={1}
                  borderColor="#D3D3D3"
                  borderWidth="1"
                  placeholder="Enter Runs"
                  placeholderTextColor="#000000"
                  color="#000000"
                  keyboardType="number-pad"
                  maxLength={4}
                  onChangeText={EnteredValue =>
                    this.ShowEmptyAlert(EnteredValue)
                  }
                  value={this.state.runs_made_end_first_innings}
                  ref={this.ref1}
                />
              </HStack>
            ) : null}
            {this.state.errorCheckEmpty2 ? (
              <View>
                <Text
                  style={{
                    color: '#ff0000',
                    fontWeight: 'bold',
                    fontFamily: fonts.ProximaNova,
                    alignSelf: 'center',
                    fontSize: 14,
                    marginRight: 2,
                  }}>
                  Runs cannot be empty
                </Text>
              </View>
            ) : this.state.errorCheck2 ? (
              <View>
                <Text
                  style={{
                    color: '#ff0000',
                    fontWeight: 'bold',
                    fontFamily: fonts.ProximaNova,
                    alignSelf: 'center',
                    fontSize: 14,
                    marginRight: 2,
                  }}>
                  Maximum runs exceeded
                </Text>
              </View>
            ) : null}
            <View>{Render_Animated_View}</View>

            {this.state.showRunsView1 == true ? (
              <HStack
                alignSelf="center"
                justifyContent="center"
                width={wp('90%')}
                bg="#FFFFFF"
                p={4}
                mt={5}
                mb={25}>
                <Text
                  style={{
                    fontFamily: fonts.ProximaNova,
                    color: '#000000',
                    fontSize: 14,
                    alignSelf: 'center',
                    width: wp('50%'),
                  }}>
                  Score at the end of 1st Innings
                </Text>
                <Input
                  ml={2}
                  textAlign="center"
                  width={wp('22%')}
                  // height={ hp("5%")}
                  alignSelf="center"
                  justifyContent="center"
                  fontSize="14"
                  p={1}
                  borderColor="#D3D3D3"
                  borderWidth="1"
                  placeholder="Enter Runs"
                  placeholderTextColor="#000000"
                  color="#000000"
                  keyboardType="number-pad"
                  maxLength={4}
                  onChangeText={EnteredValue =>
                    this.ShowEmptyAlert(EnteredValue)
                  }
                  value={this.state.runs_made_end_first_innings}
                  ref={this.ref1}
                />

                {this.state.errorCheckEmpty2 ? (
                  <View>
                    <Text
                      style={{
                        color: '#ff0000',
                        fontWeight: 'bold',
                        fontFamily: fonts.ProximaNova,
                        alignSelf: 'center',
                        fontSize: 14,
                        marginRight: 2,
                      }}>
                      Runs cannot be empty
                    </Text>
                  </View>
                ) : this.state.errorCheck2 ? (
                  <View>
                    <Text
                      style={{
                        color: '#ff0000',
                        fontWeight: 'bold',
                        fontFamily: fonts.ProximaNova,
                        alignSelf: 'center',
                        fontSize: 14,
                        marginRight: 2,
                      }}>
                      Maximum runs exceeded
                    </Text>
                  </View>
                ) : null}
              </HStack>
            ) : null}

            {/* 
            done button with condition */}
            {this.state.doneButtondisable == true ? (
              <TouchableOpacity
                disabled={this.state.doneButtondisable2}
                onPress={() => {
                  this.onPressDone();
                }}
                // state={this.state.status} onPress={this.Add_New_View_Function.bind(this)}
              >
                <View
                  alignSelf="center"
                  justifyContent="center"
                  width={wp('70%')}
                  borderWidth={2}
                  borderColor={
                    this.state.addInterruptionButtonDisable
                      ? '#AEAEAE'
                      : '#FFFFFF'
                  }
                  mt={5}
                  p={2}>
                  <HStack alignItems="center" justifyContent="center">
                    <Text
                      style={{
                        fontFamily: fonts.ProximaNova,
                        fontSize: 20,
                        textAlign: 'center',
                        color: this.state.addInterruptionButtonDisable
                          ? '#AEAEAE'
                          : '#FFFFFF',
                      }}>
                      Done
                    </Text>
                    <Image
                      style={{
                        resizeMode: 'contain',
                        marginLeft: 10,
                        marginTop: 4,
                      }}
                      source={require('@Asset/arrow-right-thin.png')}
                    />
                  </HStack>
                </View>
              </TouchableOpacity>
            ) : null}

            {/* add interruption button with condition */}
            {this.state.addInterruptionViewDisable == false ? (
              <TouchableOpacity
                disabled={this.state.addInterruptionButtonDisable}
                state={this.state.status}
                onPress={this.Add_New_View_Function.bind(this)}>
                <View
                  alignSelf="center"
                  justifyContent="center"
                  width={wp('70%')}
                  borderWidth={2}
                  borderColor={
                    this.state.addInterruptionButtonDisable
                      ? '#AEAEAE'
                      : '#FFFFFF'
                  }
                  mt={5}
                  p={2}
                  mb={25}>
                  <HStack alignItems="center" justifyContent="center">
                    <Text
                      style={{
                        fontFamily: fonts.ProximaNova,
                        fontSize: 20,
                        textAlign: 'center',
                        color: this.state.addInterruptionButtonDisable
                          ? '#AEAEAE'
                          : '#FFFFFF',
                      }}>
                      Add interruption
                    </Text>
                    <Image
                      style={{
                        resizeMode: 'contain',
                        marginLeft: 10,
                        marginTop: 4,
                      }}
                      source={require('@Asset/arrow-right-thin.png')}
                    />
                  </HStack>
                </View>
              </TouchableOpacity>
            ) : null}

            {/* 
no interruption button with condition */}
            {this.state.noInterruptionButtonDisable == false ? (
              <TouchableOpacity
                disabled={this.state.addInterruptionButtonDisable}
                onPress={() => {
                  this.noInterruption();
                }}
                // state={this.state.status} onPress={this.Add_New_View_Function.bind(this)}
              >
                <View
                  alignSelf="center"
                  justifyContent="center"
                  width={wp('70%')}
                  borderWidth={2}
                  borderColor={
                    this.state.addInterruptionButtonDisable
                      ? '#AEAEAE'
                      : '#FFFFFF'
                  }
                  p={2}
                  mb={25}>
                  <HStack alignItems="center" justifyContent="center">
                    <Text
                      style={{
                        fontFamily: fonts.ProximaNova,
                        fontSize: 20,
                        textAlign: 'center',
                        color: this.state.addInterruptionButtonDisable
                          ? '#AEAEAE'
                          : '#FFFFFF',
                      }}>
                      No interruption
                    </Text>
                    <Image
                      style={{
                        resizeMode: 'contain',
                        marginLeft: 10,
                        marginTop: 4,
                      }}
                      source={require('@Asset/arrow-right-thin.png')}
                    />
                  </HStack>
                </View>
              </TouchableOpacity>
            ) : null}
          </ScrollView>
          <View
            position="absolute"
            bottom={0}
            width={wp('100%')}
            justifyContent="center"
            alignSelf="center">
            {this.state.overs_match_start > '0' &&
            this.state.runs_made_end_first_innings > 0 &&
            this.state.errorCheckInterruption === false &&
            (this.state.addInterruptionButtonDisable === false ||
              this.state.disableSlide === true) ? (
              <SlideButton
                title="Slide to 2nd Innings"
                containerStyle={{
                  backgroundColor: '#000000',
                  borderColor: '#FFFFFF',
                  borderWidth: 2,
                }}
                titleStyle={{fontSize: 15}}
                underlayStyle={{backgroundColor: '#D3D3D3'}}
                alignSelf="center"
                autoReset
                width={wp('80%')}
                icon={
                  <Image source={require('@Asset/chevron-double-right.png')} />
                }
                marginLeft="5"
                marginRight="5"
                onReachedToEnd={() => this.submit_button()}></SlideButton>
            ) : (
              <SlideButton
                containerStyle={{
                  backgroundColor: '#000000',
                  borderColor: '#AEAEAE',
                  borderWidth: 2,
                }}
                autoReset
                title="Slide to 2nd Innings"
                titleStyle={{fontSize: 15}}
                alignSelf="center"
                position="absolute"
                bottom={0}
                underlayStyle={{backgroundColor: '#D3D3D3'}}
                disabled={true}
                icon={
                  <Image source={require('@Asset/chevron-double-right.png')} />
                }
                marginBottom="5"
                marginLeft="5"
                marginRight="5"
                width={wp('80%')}></SlideButton>
            )}
          </View>
        </View>
      </>
    );
  }
}
export default FirstInningsTwenty;
