import React, {useState,} from 'react';
import {Image,Text,PermissionsAndroid,Alert,Linking
} from 'react-native';

import {View,VStack,HStack} from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';

import RNFetchBlob from 'rn-fetch-blob';
import fonts from '../../core-libs/Fonts';

const ResultPageTwenty = (props) => {
  const {route, navigation} = props;  

  const {over_match_start} = route.params;
  const {runs_at_first_innis} = route.params;
  const {interruption_first_inng} = route.params;
  const {team_two_over_bsi_text} = route.params;
  const {penalty_run} = route.params;
  const {interruption_second_inng} = route.params;
 
  const {target_score_twenty}=route.params;
 const [targetTwenty,setTargetTwenty] = useState(target_score_twenty.TargetRuns)

const {pdf_View_Twenty}=route.params;
 const [clickedOver,setClickedOver]=useState('false');
 const [clickedBall,setClickedBall]=useState('false');
 const [pdfView,setPdfView]=useState('false');
console.log("click",clickedBall)
  console.log("over when match start 1st innnings", over_match_start);
     console.log("runs made at 1st innnings", runs_at_first_innis);
     console.log("interruption 1st innnings", interruption_first_inng);
     console.log("number of overs team 2 get before the start of second innings", team_two_over_bsi_text);
     console.log("penalty run", penalty_run);
     console.log("interruption second innings", interruption_second_inng);
     console.log("target_score222", target_score_twenty.TargetRuns);
     console.log("ballandover",pdf_View_Twenty)

    const  ballByballDownload = () => {
      const { dirs } = RNFetchBlob.fs;
     RNFetchBlob.config({
       fileCache: true,
       addAndroidDownloads: {
       useDownloadManager: true,
       notification: true,
       mediaScannable: true,
       title: `ballByballReport.pdf`,
       path: `${dirs.DownloadDir}/ballByballReport.pdf`,
       },
     })
     .fetch('GET',  pdf_View_Twenty.ballByball, {})
    .then((res) => {
      console.log('The file saved to ', res.path());
    Alert.alert(`Ball by ball report saved to 
    ${res.path()} ` );
    })
    .catch((e) => {
      console.log(e)
    });
    
       
   }
   const  overByOverDownload = () => {
    const { dirs } = RNFetchBlob.fs;
   RNFetchBlob.config({
     fileCache: true,
     addAndroidDownloads: {
     useDownloadManager: true,
     notification: true,
     mediaScannable: true,
     title: `overbyoverreport.pdf`,
     path: `${dirs.DownloadDir}/overbyoverreport.pdf`,
     },
   })
   .fetch('GET',  pdf_View_Twenty.overbyover, {})
  .then((res) => {
    console.log('The file saved to ', res.path());
  Alert.alert(`Over by over report saved to 
  ${res.path()}`);
  })
  .catch((e) => {
    console.log(e)
  });
  
     
 }
const permission = () => 
{

setClickedBall(true)
  //Function to che ck the platform
    //If iOS the start downloading
    //If Android then ask for runtime permission
    if (Platform.OS === 'ios') {
    ballByballDownload();
    } else {
      try {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title:'storage title',
            message:'storage_permission',
          },
        ).then(granted => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        
            //Once user grant the permission start downloading
            console.log('Storage Permission Granted.');
            //downloadHistory();
            ballByballDownload();
          

          } else {
            //If permission denied then show alert 'Storage Permission    Not Granted'
         Alert.alert('Storage permission denied');
          }
        });
      } catch (err) {
        //To handle permission related issue
        console.log('error', err);
      }
    }
  
  
};
const permissionOver = () => 
{
setClickedOver(true)

  //Function to che ck the platform
    //If iOS the start downloading
    //If Android then ask for runtime permission
    if (Platform.OS === 'ios') {
      overByOverDownload();
    } else {
      try {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title:'storage title',
            message:'storage_permission',
          },
        ).then(granted => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        
            //Once user grant the permission start downloading
            console.log('Storage Permission Granted.');
            
            overByOverDownload();
          

          } else {
            //If permission denied then show alert 'Storage Permission    Not Granted'
         Alert.alert('Storage permission denied');
          }
        });
      } catch (err) {
        //To handle permission related issue
        console.log('error', err);
      }
    }
  
  
};


const openPDF = (pdfUrl) => {
  // Replace with the URL of your PDF file

  Linking.openURL(pdfUrl)
    .catch((err) => console.error('An error occurred:', err));
};

const openPDFOver =(pdfUrl) => 
{
  setPdfView(true)
  
props.navigation.navigate("pdfView",{pdf_View_Twenty})
} 

  return (
   
 
    <View  flex={1}  width= {wp("100%")}
   bg='#000000'
    >
             <Image
   
        style={{width:"100%",height:"28%", resizeMode:'contain'}}

    source={require('@Asset/4.png')}
    
  />
   
    
 
        <View  alignSelf= 'center' width={wp("90%")} mt={2} justifyContent='center'>
         <VStack>
          <Text style={{fontSize:25,fontFamily:fonts.ProximaNova,
          color:'#FFFFFF' ,
         
          textAlign:'center',
         
          }}
         >Target</Text>

         <Text style={{color:'#FFFFFF',fontSize:50,fontFamily:fonts.ProximaNova,textAlign:'center', fontWeight:'bold'}}>{targetTwenty}</Text>


         {interruption_second_inng[0].RevisedOvers != ""  ?  <Text style={{color:'#FFFFFF',fontSize:20,fontFamily:fonts.ProximaNova,textAlign:'center', fontWeight:'bold'}}>in {interruption_second_inng[interruption_second_inng.length-1].RevisedOvers} Overs</Text> :  <Text style={{color:'#FFFFFF',fontSize:20,fontFamily:fonts.ProximaNova,textAlign:'center', fontWeight:'bold'}}>in {team_two_over_bsi_text} Overs</Text>}
         </VStack>
          </View>
         
        <VStack>
   
      <TouchableOpacity  onPress={() => 
           {permission(pdf_View_Twenty.ballByball) }} disabled={clickedBall===true}>
      <View alignSelf='center' justifyContent='center'  width={wp("70%")} borderWidth={2}    borderColor=     { (clickedBall===true) ? '#AEAEAE' : '#FFFFFF'} mt={5} p={2}>
        <HStack alignItems='center' justifyContent='center'  >
       <Text  style={{fontFamily:fonts.ProximaNova,fontSize:18,textAlign:'center',
       color: (clickedBall===true) ? '#AEAEAE' : '#FFFFFF'}}  >Ball by ball report</Text>
       <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/arrow-right-thin.png')}/>
       </HStack>
       </View>
      </TouchableOpacity >
      <TouchableOpacity onPress={() => 
           {permissionOver(pdf_View_Twenty.overbyover)}} disabled={clickedOver===true}>
      <View alignSelf='center' justifyContent='center'  width={wp("70%")} borderWidth={2}  borderColor=     { (clickedOver===true) ? '#AEAEAE' : '#FFFFFF'} mt={5} p={2}>
        <HStack alignItems='center' justifyContent='center'  >
       <Text  style={{fontFamily:fonts.ProximaNova,fontSize:18,textAlign:'center',color: (clickedOver===true) ? '#AEAEAE' : '#FFFFFF'}}  >Over by over report</Text>
       <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/arrow-right-thin.png')}/>
       </HStack>
       </View>
      </TouchableOpacity>
     
      </VStack>
      <TouchableOpacity onPress={() => 
           {props.navigation.navigate("HomeScreen") }}>
    <View alignSelf='center' justifyContent='center'  width={wp("70%")} borderWidth={2} borderColor='#FFFFFF' mt={100} p={2}>
        <HStack alignItems='center' justifyContent='center'  >
       <Text  style={{fontFamily:fonts.ProximaNova,fontSize:18,textAlign:'center',color:"#FFFFFF"}}  >Try another case</Text>
       <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/arrow-right-thin.png')}/>
       </HStack>
       </View>
      </TouchableOpacity>
      <View  position= 'absolute'  bottom={0} width={wp("70%")}  justifyContent='center' alignSelf='center'>

<Image

style={{width:"100%", resizeMode:'contain',alignSelf:'center',marginBottom:15,marginRight:10}}

source={require('@Asset/sporteclogo.png')}


/>


</View>

          </View>
        
      
     
 );
      

    
   
}

export default ResultPageTwenty;