import React, {useEffect, useState} from 'react';
import {
  BackHandler,
  SafeAreaView,
  TouchableOpacity,
  StatusBar,
  PermissionsAndroid,
  KeyboardAvoidingView,
} from 'react-native';
// import { Table, TableWrapper, Row, Col,} from 'react-native-table-component';
import {DataTable} from 'react-native-paper';
import {
  FlatList,
  HStack,
  Divider,
  VStack,
  Heading,
  Text,
  View,
  Box,
  Container,Content,Button
} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import TopToolbar from '../core-libs/ToolBar/TopToolBar';
import { useSelector } from 'react-redux';
import moment from 'moment';
const SubscriptionReport = props => {
  const [tableData, setTableData] = useState([]);
  const {navigation} = props;
  const {success} = props.route?.params;
  const subscripritionReport = useSelector(state => state.UserReducer?.subscriptionReport);
  function handleBackButtonClick() {
    if (navigation.canGoBack()) navigation.goBack();
    else navigation.navigate('HomeScreen');
    return true;
  }

  console.log("s@#@@#@@#@@",subscripritionReport)
  useEffect(() => {
    // Fetch data from API and update the tableData state
    if (success && success.data && success.data.response) {
      setTableData(success.data.response);
    }
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

    if (Object.keys(subscripritionReport).length != 0) {
  return (
    <>
    <View flex={1} bg="#000000"> 
      <TopToolbar
        {...props}
        onPress={handleBackButtonClick}
        text="Subscription Report"
        sourceLeft={require('@Asset/arrow-left.png')}
        sourceRight={require('@Asset/arrow-left.png')}
      />
      <Heading ml={2} mt={5} size="md" color="#FFFFFF">
        Active Plan
      </Heading>
      <FlatList
          data={subscripritionReport}  
         keyExtractor={(item, index) => {
          return index.toString();
        }}
        renderItem={( {item,index }) => (
      <Box
      borderColor='#FFFFFF'
        borderRadius={15}
        width={wp('80%')}
        borderWidth={2}
        alignSelf="center"
        mt={5}
        >
        <Text ml={2} fontSize={15} bold textAlign="center" color="#FFFFFF">
          {item.subscription_plan}
        </Text>
        <Text ml={2} fontSize={12} bold textAlign="center" color="#FFFFFF">
          RS {item.subscription_amount}/{item.timeline}days
        </Text>
        <Text ml={2} fontSize={12} bold color="#AEAEAE" textAlign="center" >
          Expires on {moment(new Date(item.end_date.toString())).format(
      'DD-MMM-YYYY hh:mm A',
    )}
        </Text>
        <Divider bg="#36640a" thickness="2" mb={1} />
        <Text></Text>
        <Text></Text>
        <Text></Text>
      </Box>

       )} 
      /> 
      </View>
    </>
  );
          }
          else {
            return (
              <>
              <View flex={1} bg="#000000" > 
                <TopToolbar
                  {...props}
                  onPress={handleBackButtonClick}
                  text="Subscription Report"
                  sourceLeft={require('@Asset/arrow-left.png')}
                  sourceRight={require('@Asset/arrow-left.png')}
                />
        {/* <View alignSelf='center' ><Text color='#FFFFFF'>No Subscriptions</Text></View> */}

        <Box flex={1} justifyContent="center" alignItems="center">
      <Text  width={wp('70%')} color="#FFFFFF">You dont have any subscriptions kindly</Text>
      <Button m={5}>Upgrade to VJD Pro</Button>
    </Box>

             
                </View>
              </>
            );
          }

};

export default SubscriptionReport;
