const React = require('react-native')
const { Platform } = React

export default {
  /** Content **/
  bgLayout: {    
  },
  imgCnt: {
    justifyContent: 'center',
    //alignItems: 'center',
    marginTop:20

  },
  bgImg: {
      //position: 'absolute',
      alignSelf:'center',
        width: 180,
       height: 140,
       tintColor: "#00008B"
       
  },
  bgImg1: {
    //position: 'absolute',
    alignSelf:'center',
    
    // width: 100,
    // height: 100
  },

  /** Header **/
  hTop: {
    flexDirection: 'column',
    alignItems: 'center',
    marginHorizontal: 15,
    marginBottom: 15
  },

  hTopLoader: {
    flexDirection: 'column',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    
     marginHorizontal: 15,


  },
  hImg: {
    fontSize: 86,
    color: '#09295A'
  },
  textValuRow:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
    marginTop:15,
    ...Platform.select({
      ios: {
        paddingVertical: 10,
      },
    }),
    paddingHorizontal: 30,
    
    // borderBottomWidth: 1,
    // borderColor: 'rgba(36,42,56,0.07)'
    
  },
  textInput:{
    borderColor:'#00008B',
    borderRadius:4,
    height:60,
   // borderWidth:1,
    flex:1,
    paddingHorizontal:10
     },
     signInBtn:{
     // borderWidth:1,
      //borderColor:'#FF8C00',
      padding:7 ,
      flex:1,
      backgroundColor:'#00008B',
      borderRadius:5
    },
  hRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  hContent: {
    justifyContent: 'center',
    marginLeft: 10
  },
  welcomeText:{
    alignSelf:'center',
    marginTop:20,
    fontSize:25,
    color:'#00008B',
    fontWeight:'bold',

  },
  hTopText: {
    marginTop:20,
    fontSize: 30,
    fontFamily: 'Amble-Regular',
    color: '#191970',
    marginBottom: 10
  },
  hTopDesc: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: '#09295A',
    marginBottom: 10
  },

  /** Form **/
  regForm: {
    width: '100%',
    marginBottom: 15
  },
  regText: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: '#FFF'
  },
  infoBox: {
    backgroundColor: '#FFF',
    elevation: 10,
    shadowOffset: {
      width: 10,
      height: 10
    },
    shadowColor: '#999',
    shadowOpacity: 0.1,
    shadowRadius: 3,
    marginHorizontal: 20,
    borderRadius: 3,
    padding: 15
  },
  fSelect: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: 'rgba(36,42,56,0.05)'
  },
  fRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
    ...Platform.select({
      ios: {
        paddingVertical: 10,
      },
    }),
    paddingHorizontal: 30,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#00008B',
    
  },
  fPicker: {
    flex: 1
  },
  fPickerItem: {
    flex: 1,
    width: '100%',
    paddingTop: 0,
    paddingBottom: 0
  },
  fIcon: {
    color: '#09295A',
    fontSize: 24,
    width: 30,
    marginRight: 5
  },
  fInput: {
    flex: 1,
    fontFamily: 'Amble-Regular',
    fontSize: 16,
    color:"#00008B"
  
    //borderBottomColor: '#09295A', 
    //borderBottomWidth: 1     
  },

  fBtn: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF8901',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 3,
    marginTop: 15
  },
  fBtnText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    color: '#FFF'
  },
  fBtnIcon: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#FFF'
  },
  forgotPassword: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    fontWeight:'bold',
    alignSelf: 'flex-end',
    color: '#09295A',
    
  },
 account: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop:10,
    marginBottom:10
  },
  accountText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    fontWeight: '900',
    color: '#00008B'
  },
  accountBtn: {
    //alignSelf: 'center',
     paddingVertical: 5,
     paddingHorizontal: 5
  },
  accountBtnText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    color: '#00008B',
    fontWeight:'bold'
  },

  connect: {
    marginHorizontal: 20,
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  connectOr: {
    flex: 1,
    width: '100%'
  },
  connectText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    marginBottom: 15,
    color: 'rgba(36,42,56,0.99)',
    alignSelf: 'center'
  },
  connectLine: {
    flex: 1,
    position: 'absolute',
    borderBottomWidth: 1,
    borderColor: '#FF0000'
  },
  connectHeader: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    color: 'rgba(36,42,56,0.99)',
    alignSelf: 'center',
    marginBottom: 15
  },
  smn: {
    flexDirection: 'row'
  },
  smnBtn: {
    flexDirection: 'row',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 3
  },
  smnIcon: {
    fontSize: 18,
    color: '#FFF',
    marginRight: 5
  },
  smnText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    color: '#FFF'
  },
  facebook: {
    backgroundColor: '#395498'
  },
  googlePlus: {
    backgroundColor: '#D64937'
  }


}
