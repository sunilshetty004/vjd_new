import React, {useState,useEffect } from 'react';
import {Image,ActivityIndicator,Text,Alert,BackHandler} from 'react-native';
import {HStack,Input,VStack,useToast,View,Button} from 'native-base';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { loginRequest} from '../../store/actions/userActions';
import { useDispatch } from 'react-redux';
import fonts from '../core-libs/Fonts';
import DeviceInfo from 'react-native-device-info';

const Login = (props) => {
  const dispatch = useDispatch();
  const toast = useToast();
  const [phoneNumber, setPhoneNumber] = useState('');
  const [isValid, setIsValid] = useState(true);

  const [mpin, setMpin] = useState('');
  const [isValidMpin, setIsValidMpin] = useState(true);
  const [showLoader, setShowLoader] = useState(false);
  const [show, setShow] = React.useState(false);
  const [uniqueId, setUniqueId] = useState('');
  const [disableLogin,setDisableLogin]= useState(false);
 
  const handleClick = () => setShow(!show);

  const showLoader1 = () => { setShowLoader(true) };




  const Login1 = () => {
   
    console.log("uniqueId#############",uniqueId)
  showLoader1
  setDisableLogin(true);
       //Login Api Calling to send necesary data
      var data = {mobile:phoneNumber,
      mpin:mpin,
      uniqueId:uniqueId
      }
   
      dispatch(loginRequest(data,(error,success)=>{
     
        if(error){
          console.log("error of mpinlogin",error.response.status);
          setShowLoader(false)
          setDisableLogin(false);
     
          if(error.response.status===401){
      
          toast.show({
            title: "Error",
            status: "Error",
     description: "Wait!! Unauthorised User",
          })
 }
 if(error.response.status===404){
      
  toast.show({
    title: "Error",
    status: "Error",
description: "Wait!! Unauthorised User",
  })
}
 if(error.response.status===403){
      
  toast.show({
    title: "Error",
    status: "Error",
description: "Oops!! Entered MPIN is wrong",
  })
}
  
if(error.response.status===412){

  toast.show({
    title: "Error",
    status: "Error",
description: "mobile number,mpin and uniqueId needed",
  })
}

 
        }
          if(success){
            setShowLoader(false)
            setDisableLogin(false);
            // console.log("token",token)
            props.navigation.navigate("HomeScreen")
            toast.show({
              title: "Welcome to VJD",
              status: "success",
              description: "Hurray!! You are all set",
            })
          }
        }))
  }
//sign up 



const signUp = () =>{

    props.navigation.navigate("LoginScreen")
    toast.show({
        title: "Sign In",
        status: "success",
        description: "Please enter your details to register",
      })  
}

const validatePhoneNumber = (number) => {
  // Perform your phone number validation logic here
  // For example, you can use regular expressions or a validation library

  // Assuming the phone number is valid if it contains 10 digits
  return /^\d{10}$/.test(number);
};
const validateMpin = (pin) => {
  // Perform your MPIN validation logic here
  // For example, you can check for the required length or specific pattern

  // Assuming the MPIN is valid if it contains 6 digits
  return /^\d{4}$/.test(pin);
};

const handlePhoneNumberChange = (number) => {
  setPhoneNumber(number);
  setIsValid(validatePhoneNumber(number));
};

const handleMpinChange = (pin) => {
  setMpin(pin);
  setIsValidMpin(validateMpin(pin));
};

const handleSubmit = () => {
  if (isValid && isValidMpin) {
    // Handle form submission or any other action
    Login1()
    console.log('Submitted phone number:', phoneNumber);
    console.log('Submitted phone number:', mpin);
  }
};
//back button press removing by this function
const handleBackPress = () => {
  // Check if you want to prevent going back in this screen
  const preventBack = true; // Set this to true or false based on your logic

  if (preventBack) {
    // Show an alert to confirm before navigating back (optional)
    Alert.alert(
      'Confirm',
      'Are you sure you want to go back?',
      [
        { text: 'Cancel', onPress: () => {} },
        {
          text: 'Yes',
          onPress: () => {  BackHandler.exitApp();
         //going out of app
          
          },
        },
      ],
      { cancelable: false }
    );

    // Return true to indicate that the back press is handled in this screen
    return true;
  }

  // If preventBack is false, return false to allow the default behavior (going back)
  return false;
};


useEffect(() => {
//device unique id 
  DeviceInfo.getUniqueId().then((uniqueId) => {
   
    console.log("serial number : DeviceInfo.getUniqueId()", uniqueId);
    setUniqueId(uniqueId)
  });

 //back button handling
 const backHandler = BackHandler.addEventListener('hardwareBackPress', handleBackPress);
 return () => backHandler.remove();

}, []);


  return (
    <>

<View bg='#000000' flex={1}  width= {wp("100%")} >
<Image style={{width:"100%",height:"28%", resizeMode: 'contain',}}
source={require('@Asset/5.png')}
/>   
<VStack width={wp("90%")} alignSelf='center' justifyContent='center'  p={5}  mt={5} >
        <Text  style={{fontFamily:fonts.ProximaNova,fontSize:25,textAlign:'center',color: '#AEAEAE' }}  >Login</Text>
        <View bg="#000000" mt={5}> 
       <Input  maxLength={10}
       size='lg'
        bg='#000000'
        borderColor='#D3D3D3'
        color='#FFFFFF' 
      borderWidth= {2}
     placeholder='Enter your number'
     keyboardType={'phone-pad'}
       value={phoneNumber}
        onChangeText={handlePhoneNumberChange}
      /> 


{!isValid && <Text style={{ color: 'red', marginTop: 10,textAlign:'center'}}>Invalid phone number</Text>}
      </View>
    <View bg="#000000"    mt={5}>         
    <Input
              maxLength={4}
              borderColor="#D3D3D3"
              size="lg"
              bg="#000000"
              borderWidth={2}
              // borderColor='#D3D3D3'
              placeholder="Enter your MPIN "
              color="#FFFFFF"
              value={mpin}
              onChangeText={handleMpinChange}
              type={show ? 'number' : 'password'}
              InputRightElement={
                <Button
                  bgColor="#000000"
                  size="xs"
                  rounded="none"
                  onPress={handleClick}>
                  {show ? 'Hide' : 'Show'}
                </Button>
              }
              // keyboardType={'phone-pad'}
            />
             {!isValidMpin && <Text style={{ color: 'red', marginTop: 10,textAlign:'center'}}>Invalid MPIN</Text>}
      </View>

      
        {((mpin.length == 4) && (phoneNumber.length == 10) && isValid && isValidMpin && (disableLogin==false) )?
       <TouchableOpacity onPress={() => Login1()}>
       <View alignSelf='center' justifyContent='center'  width={wp("70%")} borderWidth={2} borderColor='#FFFFFF'  p={2} mt={5}>
           <HStack alignItems='center' justifyContent='center'  >
          <Text  style={{fontFamily:fonts.ProximaNova,fontSize:18,textAlign:'center',color:"#FFFFFF"}}  >Log in</Text>
          <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/arrow-right-thin.png')}/>
          </HStack>
          </View>
         </TouchableOpacity>
  : 
   <TouchableOpacity disabled >
      <View alignSelf='center' justifyContent='center' width={wp("70%")} borderWidth={2}   mt={5} background='#000000' borderColor='#AEAEAE' p={2}>
        <HStack alignItems='center' justifyContent='center'  >
       <Text  style={{fontFamily:fonts.ProximaNova,fontSize:18,textAlign:'center',color: '#AEAEAE' }}  > Log in</Text>
       <Image style={{resizeMode:'contain',marginLeft:10,marginTop:4}} source={require('@Asset/arrow-right-thin.png')}/>
       </HStack>
       </View>
      </TouchableOpacity>  
      
  
    } 

     
<HStack   width={wp("90%")} mt=
{5} alignContent='center' justifyContent='center'>
          <Text  style={{fontFamily:fonts.ProximaNova,fontSize:15,color:"#808080"}}  >Dont have an account? </Text> 
      
          <TouchableOpacity onPress={() => signUp()}>
          <Text  style={{marginLeft:2,fontFamily:fonts.ProximaNova,fontSize:15,color:"#FFFFFF",}}>Register </Text></TouchableOpacity> 
          <ActivityIndicator animating={showLoader} size="large" color="white" />
</HStack>
<HStack   width={wp("90%")} alignContent='center' justifyContent='center'>
          <Text  style={{fontFamily:fonts.ProximaNova,fontSize:15,color:"#808080"}}  >Forgot MPIN? </Text> 
      
          <TouchableOpacity onPress={() => signUp()}>
          <Text  style={{marginLeft:2,fontFamily:fonts.ProximaNova,fontSize:15,color:"#FFFFFF",}}>Click here</Text></TouchableOpacity> 
          <ActivityIndicator animating={showLoader} size="large" color="white" />
</HStack>

      </VStack>
     
        


      <View  position= 'absolute'  bottom={0} width={wp("70%")}  justifyContent='center' alignSelf='center'>

<Image

style={{width:"100%",resizeMode:'contain',alignSelf:'center',marginBottom:15,marginRight:10}}

source={require('@Asset/sporteclogo.png')}


/>


</View>
      </View>
  </>
 
  );

  

    
}
export default Login;



