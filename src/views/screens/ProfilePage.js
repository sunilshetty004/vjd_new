import React, {useEffect, useState} from 'react';
import {
  //  StatusBar,
  ScrollView,
  SafeAreaView,
  BackHandler,
  RefreshControl,
  Image,
} from 'react-native';

import {Center, View, Box, useToast} from 'native-base';
import ProfileHeader from '../core-libs/ToolBar/ProfileHeader';
import RectangleCards from '../core-libs/CardView/RectangleCards';
import TopToolbar from '../core-libs/ToolBar/TopToolBar';
import {useDispatch, useSelector} from 'react-redux';
import {
  getPaymentReportRequest,
  getSubscriptionReportRequest,
} from '../../store/actions/userActions';
import {backgroundColor} from 'styled-system';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ProfilePage = props => {
  const {navigation} = props;
  const [isFetching, setIsFetching] = useState(false);

  const toast = useToast();
  const dispatch = useDispatch();
  //   const {success} =props.route?.params;
  //   console.log("Profile Result",success);
  const onPressPaymentReport = () => {
    dispatch(
      getPaymentReportRequest((error, success) => {
        if (error) {
          toast.show({
            title: 'Error',
            status: 'Error',
            description: 'Error while fetching data',
          });
          return;
        }
        if (success) {
          props.navigation.navigate('PaymentReport', {
            success,
          });
          console.log('paymentreport###########', success);
        }
      }),
    );
  };

  const onPressSubscriptionPage = () => {
    dispatch(
      getSubscriptionReportRequest((error, success) => {
        if (error) {
          toast.show({
            title: 'Error',
            status: 'Error',
            description: 'Error while fetching data',
          });
          return;
        }
        if (success) {
          props.navigation.navigate('SubscriptionReport', {
            success,
          });
          console.log('subscription3433', success.data);
        }
      }),
    );
  };
  const onPressOrderPage = () => {
    props.navigation.navigate(NavConstants.orderScreen);
  };
  const onRefresh = () => {
    setIsFetching(true);
    setIsFetching(false);
    //  this.setState({isFetching: true,},() => {this.getApiData();});
  };
  function handleBackButtonClick() {
    if (navigation.canGoBack()) navigation.goBack();
    else navigation.navigate(NavConstants.home);
    return true;
  }
  useEffect(() => {
    setIsFetching(false);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);
  return (
    <>
      <TopToolbar
        {...props}
        onPress={handleBackButtonClick}
        text="My Account"
        sourceLeft={require('@Asset/arrow-left.png')}
        sourceRight={require('@Asset/arrow-left.png')}
      />

      <SafeAreaView
        style={{
          backgroundColor: '#000000',
          borderRadius: 10,
          padding: 10,
          flex: 1,
        }}>
        {/* <StatusBar backgroundColor="#f4f4f4" animated barStyle='dark-content' /> */}

        <ScrollView
          // style={Style.scrollView}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={isFetching} onRefresh={onRefresh} />
          }>
          <Center fbg="#000000">
            <Box width="100%">
              <View>
                <ProfileHeader {...props} />

                <RectangleCards
                  text="Payment History"
                  sourceLeft={require('@Asset/edit.png')}
                  mb={2}
                  size={25}
                  alt="img"
                  onPress={onPressPaymentReport}
                />
                <RectangleCards
                  text="Subscription Histoty"
                  sourceLeft={require('@Asset/edit.png')}
                  onPress={onPressSubscriptionPage}
                  mb={2}
                  size={25}
                  alt="img"
                />
                <RectangleCards
                  text="{CardText.payment}"
                  sourceLeft={require('@Asset/edit.png')}
                  onPress={onPressOrderPage}
                  mb={2}
                  size={25}
                  alt="img"
                />
              </View>
            </Box>
          </Center>
        </ScrollView>
        <View
          position="absolute"
          bottom={0}
          width={wp('70%')}
          justifyContent="center"
          alignSelf="center">
          <Image
            style={{
              width: '100%',
              resizeMode: 'contain',
              alignSelf: 'center',
              marginBottom: 15,
              marginRight: 10,
            }}
            source={require('@Asset/sporteclogo.png')}
          />
        </View>
      </SafeAreaView>
    </>
  );
};
export default ProfilePage;
