const React = require('react-native')
const { Platform, Dimensions } = React

const deviceHeight = Dimensions.get('window').height
const deviceWidth = Dimensions.get('window').width

export default {

  layout: {
    flex: 1
  },
  nav: {
    flex: 1
  },
  navProfile: {
    flex: 2,
    justifyContent: 'center',
    alignItems:"center",
    paddingHorizontal: 20,
  },
  bgImg: {
    position: 'absolute',
    width: '120%',
    height: 230
  },
  navImg: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },

  navMenu: {
    flex: 8,
    // paddingTop: 15,
    paddingBottom: 15,
    paddingHorizontal: 5
  },
  navAvatar: {
    width: 85,
    height: 85,
    borderRadius: 45,
    alignSelf: "center",
    justifyContent: 'space-between',
    
    overflow: "hidden",
    borderColor:"#aeaeae",
    borderWidth:1,
  },
  navName: {
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    color: '#00008B',
    marginTop: 5,
    fontWeight:'bold'
  },

  profileItem: {
    marginTop: 10,
    marginBottom: 10
  },
  navBtn: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  
  navBtnIcon: {
    fontSize: 24
  },
  navBtnLeft: {
    width: 30,
    marginRight: 20
  },
  navBtnText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#00008B',
    fontWeight:'bold',
  },
  navHeader: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    color: '#242A38',
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20
  },
  navFooter: {
    flex: 1
  },
  navFooterText: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: 'rgba(0,0,0,0.5)'
  }

}
