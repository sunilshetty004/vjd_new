import React, { useEffect, } from 'react';
import Spinner from 'react-native-spinkit';
import { useSelector, useDispatch } from 'react-redux';
import { View,Alert } from 'react-native';
import { Box,useToast,Text } from 'native-base';
import { hideLoader } from '../../../store/actions/utilActions';// Import the hideLoader action creator

const Loader = () => {
  const dispatch = useDispatch();
  const toast = useToast();
  const loader = useSelector((state) => state.UtilsReducer);
  const { isLoading = false } = loader;
console.log("loader",loader)

  useEffect(() => {
    if (isLoading) {
      const timeout = setTimeout(() => {
        dispatch(hideLoader()); // Dispatch the hideLoader action after the specified duration
      }, 30000); // Set the duration (in milliseconds) after which the loader should automatically hide

      return () => {
        clearTimeout(timeout); // Clear the timeout if the component unmounts before the timeout is reached
      };
    }
  }, [isLoading, dispatch]);

  
  
  return (
    <>
      {
      isLoading ? (
        <View style={{ position: 'absolute', alignItems: 'center', zIndex: 11, justifyContent: 'center', width: '100%', height: '100%', backgroundColor: "#000000" }}>
          <Spinner
            color={'#FFFFFF'}
            size={100}
            type={'Wave'}
          />
        </View>
      )
    :

    
 null
    }
    </>
  );
};

export default Loader;
