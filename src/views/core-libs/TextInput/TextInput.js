import React from 'react';
import {Input} from "native-base"

const TextInputView = ({value,underlined, textContentType, keyboardType,maxLength, placeholder, onChangeText,width, height,onSubmitEditing,inputRef,secureTextEntry}) => {
    return(
      <Input
      width={width}
      h={height}
        
        variant="underlined"
        color="#32CD32"
        backgroundColor="#FFFFFF"
        fontSize={15}
        mt='1%'
       // paddingLeft='15%'
        borderRadius={15}
        placeholder={placeholder}
        textContentType={textContentType}
        keyboardType={keyboardType}
        maxLength={maxLength}
        autoCorrect= {false}
      secureTextEntry={secureTextEntry}
        value={value}
        onSubmitEditing={onSubmitEditing}
        ref={inputRef}
        onChangeText={onChangeText}
        _light={{
          placeholderTextColor: '#8a8686',
          placeholderfontFamily: "AnotherFont", 
          placeholderborderColor: '#12222222' 
        }}
        _dark={{
          placeholderTextColor: '#8a8686',
          placeholderfontFamily: "AnotherFont", 
          placeholderborderColor: '#12222222' 
        }}
       
      />
    );
}
export default TextInputView;
