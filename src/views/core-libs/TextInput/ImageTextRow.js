import React from 'react';
import { HStack, Text, Image} from 'native-base';
const ImageTextRow = ({sourceLeft, text, sourceRight, mb, size}) => {
    return(
  <>
  <HStack  alignItems="center" mb={mb}>
  <Image
          source={sourceLeft}
          alt="img"
          size={size}
        />
       
       <Text
            style={{
                  // borderRadius:80,
                   color: '#000',
                   fontSize: 15,
                   alignSelf:'center',
                   textAlign:'center',
                   paddingHorizontal:'2%'
                   }}
               bold ml={1}
                   
          
          >{text}</Text>
          </HStack>
   
</>
    );
}
export default ImageTextRow;