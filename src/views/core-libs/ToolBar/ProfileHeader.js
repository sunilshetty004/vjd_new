import React,{ useEffect , useState} from 'react';
import {
    StyleSheet,
    TouchableOpacity
  } from 'react-native';
import {
  Image,
  View,
  Text,
  Box,
  Center,
  Divider,
  HStack,
   useToast,
} from 'native-base';
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

 
  import { useDispatch, useSelector } from 'react-redux';
  import { viewProfileRequest } from '../../../store/actions/userActions';
//   import {requestViewProfile} from '../../../store/actions/userActions';
import { ROOT_URL } from '../../../constants/urlConstatns';
  import { useIsFocused } from "@react-navigation/native";
  import moment from 'moment';
const ProfileHeader = (props) => {
  
  const userData = useSelector(state => state.UserReducer?.userData);
  const viewProfile = useSelector(state => state.UserReducer?.viewProfile);
  const [userFirstName, setUserFirstName] = useState('');
  const [userLastName, setUserLastName] = useState('');
  const [userMobileNumber, setUserMobileNumber] = useState('');
  const [userProfilePhoto, setUserProfilePhoto] = useState(null);
  const [userGender, setUserGender] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const toast = useToast();
  const [date_of_birth, setDate_of_birth] =useState(null);
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const userId = useSelector(state => state.UserReducer?.userData?.userId);
  const onPressProfileEdit=()=>{
    props.navigation.navigate("ProfileEditScreen",
       {
         first_name1: userFirstName,
      
         mobile1: userMobileNumber,
        photo1: (userProfilePhoto != "null" || userProfilePhoto != null) ? userProfilePhoto : null,
        gender1: userGender,
         email1: userEmail,
        date_of_birth1:(date_of_birth != "null" || date_of_birth != null) ? date_of_birth : null
         } );
    // if(userData != null){
    // //   props.navigation.navigate(NavConstants.profileEditScreen,
    // //     {
    // //      first_name1: userFirstName,
    // //      last_name1: userLastName,
    // //      mobile1: userMobileNumber,
    // //      photo1: (userProfilePhoto != "null" || userProfilePhoto != null) ? userProfilePhoto : null,
    // //      gender1: userGender,
    // //      email1: userEmail,
    // //      date_of_birth1:(date_of_birth != "null" || date_of_birth != null) ? date_of_birth : null
    // //     } );
    // console.log("Sunil")
    //   }else{
    //    // props.navigation.navigate(NavConstants.login); 
    //     toast.show({
    //       render: () => {
    //         return (
    //           <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
    //            You Have Not Logged-In, Please Login
    //           </Box>
    //         )
    //      },
    //      duration:2000,
    //     })
    //   }
   
 }

 
//  const onViewProfileSubmit =()=>{
//   // var dataValue ={userid: userId}
//       dispatch(requestViewProfile((err,success)=>{
//        if(err){
//         console.log('err----rrr',err.response)
//       //  const { status } = err.response;
//         console.log("statussssssssssss;;",status);
//         // switch (status) {
//         //   case 401:
           
//         //     break;
//         //   case 400:
            
//         //        break;
//         //   case 500:
           
//         //     break;
//         //   case 404:
//         //     toast.show({
//         //       title: "Invalid credentials",
//         //       status: "warning",
//         //       description: "Please enter a valid data",
//         //     })
//         //       break;
//         //   default:
//         //     return "";
//         // }
//          return;
//        }
   
//       if (success.data.status == 200){
//         setUserFirstName(success.data.response[0].first_name);
//         setUserLastName(success.data.response[0].last_name);
//         setUserMobileNumber(success.data.response[0].mobile);
//         setUserProfilePhoto(success.data.response[0].photo);
//         setUserGender(success.data.response[0].gender);
//         setUserEmail(success.data.response[0].email);
//         setDate_of_birth(success.data.response[0].date_of_birth)
//       } 
//     //   if (success.data.status == 404){
//     //     console.log("status success",success.data.status);
//     //     console.log("status message", success.data.message);
//     //     toast.show({
//     //       title: "Invalid credentials",
//     //       status: "warning",
//     //       description: "Please enter a valid data",
//     //     })
//     //    props.navigation.navigate(NavConstants.login)
//     //  }  
//      }
//     ))
 
// }
const onViewProfile= ()=>{
    dispatch(viewProfileRequest((error, success) => {
      if (error) {
        
        toast.show({
          title: "Error",
          status: "Error",
   description: "Error while fetching data",
        })
         return;
      }
      if (success) {

        
        
        
      
        // console.log("successsuccess###########",success[0].photo)
// props.navigation.navigate("ProfilePage",{success});
  }
   
    }))




  }
 
useEffect(() => {
  onViewProfile();
  setUserFirstName(viewProfile[0]?.username);
  setUserMobileNumber(viewProfile[0]?.mobile);
  setUserProfilePhoto(viewProfile[0]?.photo);
  setUserGender(viewProfile[0]?.gender);
  setUserEmail(viewProfile[0]?.email);
  setDate_of_birth(viewProfile[0]?.date_of_birth)
 
}, [isFocused])
console.log("sss",date_of_birth)
  return (
   <View >
 
          <View  bg= '#FFFFFF' mt = {hp('2%')} mx={2} borderRadius= {20}>
            {/* Profile Details */}
           
            
              {/* Profile Image */}
              <TouchableOpacity
              //style={{marginTop:'20%'}}
                   onPress={onPressProfileEdit}      
                >
             <Image
           
              source={require('@Asset/edit.png')}
              alt="img"
            alignSelf='flex-end'
            //mt="10%"
            mr={2}
            mt={hp('2%')}
            // alt='img'
              w={25}
              h={25}
            />
            </TouchableOpacity>
              <View alignItems= 'center'>
              
             
              {(userProfilePhoto !== null)&&
              (<Box
               w={100}
                h={100}   borderRadius={50} bg='#000' mt={10}>
                  <Image
               size={100}
                h={100}
                borderRadius={100}
                 value={userProfilePhoto}
                  resizeMode='cover'
                  source={ { uri:`${ROOT_URL}${userProfilePhoto}` }}
               // source={require('@Asset/profile_icon.png')} 
                  alt="img"
              //    { uri: `${ROOT_URL}${userProfilePhoto}` }
                />
                </Box>)
                  }
              {(userProfilePhoto == null)&&
              <Image
                  source={require('@Asset/profile_icon.png')} 
                  value={userProfilePhoto}
                  size={100}
                  alt='img'/>
                  }
              
          
            
             
              </View>
              
              <Center>
              <Text fontSize={20} bold color="#000"  mt={2} ml={3}>{userFirstName}</Text>
              <Divider my={1} bg='rgba(36,42,56,0.07)' w='95%'/>
               <HStack mx={4}>
               <Text fontSize={14} color="#000" w='70%' mt={1} ml={3}>Email: {(userEmail != "") ? userEmail : "-----"}</Text>
              <Text fontSize={14} color="#000" w='30%'  mt={1} ml={2}mr={3} alignSelf='flex-end'>{userGender}</Text>
              </HStack>

              <Divider my={1} bg='rgba(36,42,56,0.07)' w='95%'/>

              <HStack mx={4}>
               <Text fontSize={13} color="#000" w='60%' mt={1} ml={3}>Age:  {moment().diff(date_of_birth, 'years')}</Text>
              <Text fontSize={13} color="#000" w='40%'  mt={1} ml={2}mr={3} alignSelf='flex-end'>+91 {userMobileNumber}</Text>
              </HStack>
              <Divider my={1} bg='rgba(36,42,56,0.07)' w='95%' mb='10%'/>

      
              </Center>
              </View>
</View>


    

          

             

 
 
    
     
      
  );
}
const styles = StyleSheet.create({
  header:{
    justifyContent: "center", 
    alignItems: "center",
  },
  textRow:{
    flexDirection:'row',
     width:wp("100%"), 
     marginBottom:'2%'
  },
  textName:{
    marginTop:"7%",
     marginLeft:"4%", 
     color:"#495d87", 
     fontFamily:'@Asset/Amble-Bold',
      fontSize:24, fontWeight:'bold'
  },
  profileContainer: {
    // height: 1000,
   
  },
  profileImageView: { 
 },
  coverImage: { height: 300, width: '100%' },
  profilePicture: {
   // paddingTop: '20%',
    position: 'absolute',
    zIndex: 1,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileImage: {
    
  },
  textGender:{
    width:wp("35%"),
     marginTop:"2%", 
     color:"#495d87", 
     fontFamily:'@Asset/Amble-Bold', 
     fontSize:15,
      marginRight:"4%"
  },
  textId:{
    width:wp("65%"),
     marginTop:"2%", 
     marginLeft:"4%", 
     color:"#495d87", 
     fontFamily:'@Asset/Amble-Bold', 
     fontSize:15, 
  },
  textMobile:{
    marginTop:"2%", 
    color:"#495d87", 
    fontFamily:'@Asset/Amble-Bold',
     fontSize:14, 
     marginRight:"4%",
      alignSelf:'flex-end',
       marginBottom:'4%'
  },
    iconsSize: {
        width: wp("35%"), 
        height: hp("30%"),
        tintColor: "#788fbf"
       },
        fRow: {
            paddingHorizontal: "50%",
            borderBottomWidth: 1,
            borderColor: 'rgba(36,42,56,0.07)',
            marginTop:"2%"
          },  
          cardContainer: {
            backgroundColor: '#FFFFFF',
            borderRadius: 2,
            elevation: 20,
           },
           camButton: {
            position: 'relative',
             overflow: 'hidden',
             width: 50,
             height: 50,
             borderRadius: 100,
             top: -8,
             overflow: "hidden",
             borderColor:"#aeaeae",
             borderWidth:1,
             backgroundColor:"#FFF"
         
             
           },
           camTouch: {
             textAlign: "center",
             width: "15%",
             height: '20%',
             left: 70,
             top: -40
           },
  });
export default ProfileHeader;