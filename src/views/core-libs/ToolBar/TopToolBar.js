import React from 'react';

import { 
  TouchableOpacity,
 } from 'react-native';


import { HStack, Text, ArrowBackIcon, Box, Image, View,  AppBar, Badge} from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { NavConstants } from '../../../constnts/navConstants';
// import { useSelector } from 'react-redux';
const TopToolbar = (props) => {
  const {navigation, cartNum,text ,onPress} = props;
  // const cartItems = useSelector(state => state.cartItems.cartItems);
  // console.log("cartItems top toolbar", cartItems);
 
  return (
    <HStack bg="#DEDEDE" elevation= {5} w={wp('100%')} p={3}>

    <TouchableOpacity onPress={onPress}>
 <ArrowBackIcon  color= '#000000' alt='' size='25' />
</TouchableOpacity>
    <View w={wp('65%')} mt={1}>
    <Text color='#000' textAlign='center' alignSelf='center' fontSize={15} bold >{text}</Text>     

    </View>  
    {/* <HStack w={wp('25%')} ml={1}> */}
    {/* <TouchableOpacity
  onPress={() => {
    props.navigation.navigate(NavConstants.cartScreen,{selected_coupon:'', final_amounts:'', coupon_id:''} );
}}>
    <View position='absolute' h={25} w={25} borderRadius={15} bg='#6f31ad' right={3} bottom={15} alignItems='center' justifyContent='center' zIndex={2000}>
    <Text color='#FFFFFF' fontSize={12} bold>{cartItems.length}</Text>
    {console.log("cartItems in topToolbar", cartItems.length)}
  </View>
  <Image  justifyContent='center'
          source={require('@Asset/cart.png')}
           alt="img" 
         justifyContent='flex-end'
         alignSelf='center'
         />   
            </TouchableOpacity>                 */}
                          {/* <Badge resizeMode={"contain"}  borderRadius={100} bg='#ff7c38' 
          colorScheme='#ff7c38'style={{ position: "absolute", top:'-5%', left: '10%' }}>
                            <Text  color='#FFFFFF'  h={5} textAlign='center'>{cartNum}</Text>
                          </Badge> */}
        {/* <TouchableOpacity style={{alignSelf:'center'}} 
                       onPress={() => {
                        navigation.navigate(NavConstants.notification);
                   }}
                >
     <Image 
           source={require('@Asset/bell-outline.png')}
           alt="img"
           ml={1}
        />      
        </TouchableOpacity>       */}
    {/* </HStack> */}
    
            </HStack> 



       
    
        
  
  );
}

export default TopToolbar;