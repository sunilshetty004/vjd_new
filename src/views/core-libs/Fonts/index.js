import { Platform } from "react-native";
const fonts ={
    AquireBold:Platform.OS==="ios"  ? 'aquirebold' :'aquirebold',
    ProximaNova:Platform.OS==="ios" ? 'proxima' : 'proxima' ,
    FastTrack:Platform.OS==="ios" ? 'fasttrack' : 'fasttrack' 
};
export default fonts;