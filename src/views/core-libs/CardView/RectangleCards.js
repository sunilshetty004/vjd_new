import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import { Box, Stack} from 'native-base';
 import ImageTextRow from '../TextInput/ImageTextRow';
const RectangleCards = ({text, sourceRight, onPress, sourceLeft, mb, size}) => {
    return(
    <>
     
     <Stack>
    <TouchableOpacity
    onPress={
      onPress
      }>
    <Box
        bg="#FFFFFF"
        rounded="md"
        alignSelf="center"
        width="95%"
        p={4}
        maxWidth="100%"
        marginTop={2}
        elevation={1}
        space={4}
       // rounded="lg"
        mb={mb}>
    

  <ImageTextRow text={text} sourceLeft={sourceLeft} sourceRight={sourceRight} size={size} /> 
 
    </Box>
    </TouchableOpacity>

    </Stack>
</>
    );
}
export default RectangleCards;