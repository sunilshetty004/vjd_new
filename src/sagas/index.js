import { all, fork } from "redux-saga/effects";
import * as userSaga from "./userSaga";

export default function* rootSaga() {
    yield all(
      [
      ...Object.values(userSaga),
      // ...Object.values(DashbaordSaga),
      // ...Object.values(MenuItemsSaga),
      ].map(fork)
    );
  }