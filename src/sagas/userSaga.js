/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
import React, {useEffect, useState, useRef} from 'react';
import {Alert } from 'react-native';
import { put, takeLatest,takeEvery, call, select } from 'redux-saga/effects';
import * as types from '../constants/actionTypeConstants';
import { API_URL } from '../constants/urlConstatns';
import { API } from '../services/API/networkAPI';
import _ from 'underscore';
import {isLoading} from '../store/actions/utilActions';

import {loginSuccess, loginFailure, 
  logOutSuccess,logOutFailure, 
    targetFailure,targetSuccess,
    targetFailureTwenty,targetSuccessTwenty, pdfViewSuccess, pdfViewFailure, pdfViewTwentySuccess, pdfViewTwentyFailure,signInSuccess,signInFailure, subscribeSuccess,subscribeFailure, verifySubscribeSuccess, verifySubscribeFailure,getAllSubscriptionsSuccess, getAllSubscriptionsFailure, viewProfileSuccess, viewProfileFailure, editProfileSuccess, editProfileFailure, getPaymentReportSuccess,getPaymentReportFailure, getSubscriptionReportSuccess, getSubscriptionReportFailure, getProUsertRequest, getProUserSuccess, getProUserFailure, unAuthorizedSuccess, unAuthorizedFailure,navigateToLogin
} from '../store/actions/userActions';
console.log("URL",API_URL.targetscoreTwenty)

const toast1 = () => {
    Alert.alert('Server Timeout', 'Kindly try again after sometime', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ]);
  
  }
//register
function* onSignInSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of login',payload);
        yield put(isLoading(true));

        const result = yield call(API.post, API_URL.register, payload);
        yield put(signInSuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(signInFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}


//login
function* onLoginSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of login',payload);
        yield put(isLoading(true));

        const result = yield call(API.post, API_URL.login, payload);
        yield put(loginSuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(loginFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}

//TARGET SCORE
function* onTargetSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of target',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)
        const result = yield call(API.post, API_URL.targetscore,payload,headers);

        console.log("DSShfkdus",result.data)
        yield put(targetSuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(targetFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}



//TARGET SCORE Twenty
function* onTargetTwentySaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of targetTwenty',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.targetscoreTwenty,payload,headers);
        yield put(targetSuccessTwenty(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);          
         yield put(targetFailureTwenty(e));
        yield put(isLoading(false));

        // return e;

    }
}

//pdf view
function* onPdfViewSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of pdfView',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.pdfView,payload,headers);
        yield put(pdfViewSuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(pdfViewFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}




//pdf view
function* onPdfViewTwentySaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of pdfViewTwenty',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.pdfViewTwenty,payload,headers);
        yield put(pdfViewTwentySuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(pdfViewTwentyFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}

//subscribe
function* onSubscribeSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of subscribe',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.subscribe, payload,headers);
        yield put(subscribeSuccess(result));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(subscribeFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}


//subscription verification
function* onVerifySubscribeSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of subscribe',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.verifySubscription,payload,headers);
        yield put(verifySubscribeSuccess(result));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(verifySubscribeFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}



///get all subscriptions


function* getAllSubscriptionsSaga(action) {
    const {payload,callback} = action;
    try {
        yield put(isLoading(true));


        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
        "Content-Type": "application/json"
            //"Content-Type": "multipart/json"
        };

        
        const result = yield call(API.post, API_URL.getAllSubscriptions,payload,headers);
        console.log("result of get All subscrip###LLL", result?.data?.response);
       
        yield put(getAllSubscriptionsSuccess(result?.data?.response));
        callback(null, result?.data?.response);
        // console.log("result of get getAllSubscriptionsSuccess subscrip###LLL",result.data);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(getAllSubscriptionsFailure(e));
        yield put(isLoading(false));
    
        // return e;

    }
}



//view profilw
function* onViewProfileSaga(action) {
    const {callback} = action;


    try {
        yield put(isLoading(true));


        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            // "Content-Type": "application/json"
            "Content-Type": "multipart/json"
        };

        
        const result = yield call(API.get, API_URL.viewprofile,headers);
        console.log("result of get All view profile###LLL", result?.data?.response);
       
        yield put(viewProfileSuccess(result?.data?.response));
        callback(null, result?.data?.response);
       
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(viewProfileFailure(e));
        yield put(isLoading(false));
    
        // return e;

    }
}


//edit Profile
function* onEditProfileSaga(action) {
    const { payload, callback } = action;


    try {
        console.log('payload of onEditProfileSaga',payload);
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "multipart/form-data"
        };
        console.log("headers",headers)

        const result = yield call(API.post, API_URL.editprofile,payload,headers);
        yield put(editProfileSuccess(result.data));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);          
         yield put(editProfileFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}

///get Payment report
function* getPaymentReportSaga(action) {
    const { callback } = action;


    try {
 
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.get, API_URL.paymentReport,headers);
        console.log("result.data.response############################################################################################",result.data)
        yield put(getPaymentReportSuccess(result.data.response));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(getPaymentReportFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}



////GET_SUBSCRIPTION_REPORT
function* getSubscriptionReportSaga(action) {
    const { callback } = action;


    try {
 
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.get, API_URL.subscriptionReport,headers);
        console.log("result.datasubscriptionreport.",result.data)
        yield put(getSubscriptionReportSuccess(result.data.response));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        
        yield put(getSubscriptionReportFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}

//getProUserSaga
function* getProUserSaga(action) {
    const { callback } = action;


    try {
 
        yield put(isLoading(true));
        const store = yield select()
        const headers = {
            Authorization: "Bearer " + store?.UserReducer?.token,
            "Content-Type": "application/json"
        };
        console.log("headers",headers)

        const result = yield call(API.get, API_URL.getProuser,headers);
        console.log("result.getProuser.",result.data)
        yield put(getProUserSuccess(result.data.response));
        callback(null, result);
        yield put(isLoading(false));


    } catch (e) {
        callback(e, null);
        yield put(getProUserFailure(e));
        yield put(isLoading(false));

        // return e;

    }
}
//loadhider
function* hideLoaderSaga() {
    yield put(isLoading(false));
    toast1()
    
  }
  //unauthorized re
  function* unAuthorizedSaga(payload) {
 
    console.log("data logout saga", payload)
    try {
        // props.navigation.navigate("Login")
        // yield put(unAuthorizedSuccess());
      
        yield put(navigateToLogin());//login 
        persistor.purge();
    //   callback(null,data);
    }
    catch(error) {
    //   yield put(unAuthorizedFailure());
      console.log("ereiunAuthorizedFailure",error)
    //   callback(error,null);
    //toaster("error", error);
    }
  }

//logout
// function* logoutSaga() {
//     const {callback} = action;
//     // const {data,callback} =payload;
//     // const {data} = payload;
//   //  console.log("data logout saga", payload)
//     try {
//        // yield put(isLoading(true));
//         persistor.purge();
//        // yield put(isLoading(false));
//       yield put(logOutSuccess());

//     //   callback(null,data);
//     }
//     catch (error) {
//        // yield put(isLoading(false));
//       yield put(logOutFailure());
//     //   callback(error,null);
//     //toaster("error", error);
//     }
//   }
// function* logoutSaga(action) {
//     const {callback} = action;


//     try {
//         yield put(isLoading(true));


//         const store = yield select()
//         const headers = {
//             Authorization: "Bearer " + store?.UserReducer?.token,
//             // "Content-Type": "application/json"
//             "Content-Type": "multipart/json"
//         };

        
//         const result = yield call(API.get, API_URL.logout,headers);
//         // console.log("result of get All subscrip###LLL", result?.data?.response);
//         persistor.purge();
//         yield put(logOutSuccess(result));
      
//         callback(null, result);
//         // console.log("result of get getAllSubscriptionsSuccess subscrip###LLL",result.data);
//         yield put(isLoading(false));


//     } catch (e) {
//         callback(e, null);
//         yield put(logOutFailure(e));
//         yield put(isLoading(false));
    
//         // return e;

//     }
// }
function* logoutSaga(action) {
    const {payload,callback} = action;
   console.log("data sent to verify logout  saga", payload)
    try {
        
        yield put(isLoading(true));
        // const store = yield select()
        // const headers = {
        //     Authorization: "Bearer " + store?.UserReducer?.token,
        //     "Content-Type": "application/json"
        // };
        // console.log("token----verify logout saga",store?.UserReducer?.token)
        const result = yield call(API.post, API_URL.logout, payload);
        // persistor.purge();
        console.log("verify logout saga------", result);
        yield put(logOutSuccess(result));
        callback(null, result);
        
        yield put(isLoading(false));
     
    }
    catch(error) {
      yield put(isLoading(false));
      yield put(logOutFailure(error));
      callback(error,null);
     
      // toaster("error", error);
    }

   
       
  }




  
  export function* onSignInSagaWatcher() {
    yield takeLatest(types.SIGN_IN_REQUEST, onSignInSaga);
}
export function* onLoginSagaWatcher() {
    yield takeLatest(types.LOG_IN_REQUEST, onLoginSaga);
}

export function* onTargetSagaWatcher() {
    yield takeLatest(types.TARGET_REQUEST,onTargetSaga);
}
export function* onTargetTwentySagaWatcher() {
    yield takeLatest(types.TARGET_REQUEST_TWENTY,onTargetTwentySaga);
}
export function* onPdfViewSagaWatcher() {
    yield takeLatest(types.PDF_VIEW_REQUEST,onPdfViewSaga);
}

export function* onPdfViewTwentySagaWatcher() {
    yield takeLatest(types.PDF_VIEW_TWENTY_REQUEST,onPdfViewTwentySaga);
}

export function* onSubscribeSagaWatcher() {
    yield takeLatest(types.SUBSCRIBE_REQUEST,onSubscribeSaga);
}

export function* onVerifySubscribeSagaWatcher() {
    yield takeLatest(types.VERIFY_SUBSCRIBE_REQUEST,onVerifySubscribeSaga);
}

export function* getAllSubscriptionsSagaWatcher() {
    yield takeLatest(types.GET_ALL_SUBSCRIPTIONS_REQUEST,getAllSubscriptionsSaga);
}


export function* onViewProfileSagaWatcher() {
    yield takeLatest(types.VIEW_PROFILE_REQUEST,onViewProfileSaga);
}

export function* onEditProfileSagaWatcher() {
    yield takeLatest(types.EDIT_PROFILE_REQUEST,onEditProfileSaga);
}

export function* getPaymentReportSagaWatcher() {
    yield takeLatest(types.GET_PAYMENT_REPORT_REQUEST,getPaymentReportSaga);
}


export function* getSubscriptionReportSagaWatcher() {
    yield takeLatest(types.GET_SUBSCRIPTION_REPORT_REQUEST,getSubscriptionReportSaga);
}

export function* getProUsertSagaWatcher() {
    yield takeLatest(types.GET_PROUSER_REQUEST,getProUserSaga);
}


export function* hideLoaderSagaWatcher() {
    yield takeLatest(types.HIDE_LOADER, hideLoaderSaga);
  }
  export function* unAuthorizedSagaWatcher() {
    yield takeLatest(types.NAVIGATE_TO_LOGIN,unAuthorizedSaga);
}
export function* logoutSagaSagaWatcher() {
    yield takeLatest(types.LOG_OUT_REQUEST,logoutSaga);
}