import axios from 'axios';
// import errorPage from './RedirectErrorPage';
import errorScreen from '../../constants/ErrorHandlerReDirectionScreen';

export const API = {
  get: (route,headers) => {
    const getPromise = new Promise((resolve, reject) => {
      API.prepareConfig(route, null, 'get', headers, (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
      });
    });
    return getPromise;
  },
  post: (route, params, headers) => {
    const postPromise = new Promise((resolve, reject) => {
      API.prepareConfig(route, params, 'post', headers, (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
      });
    });
    return postPromise;
  },

  put: (route, params, headers, callback) => API.prepareConfig(route, params, 'put', headers, callback),
  delete: (route, params, headers) => {
    const getPromise = new Promise((resolve, reject) => {
      API.prepareConfig(route, params, 'delete', headers, 
     // isCustomerError,
       (err, res) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(res);
      });
    });
    return getPromise;
  },

  prepareConfig: async (routeurl, params, methodType, headers={}, callback) => {

    const config = {
      method: methodType,
      url: routeurl,
      data: params,
      headers: headers,
      // timeout: API.requesttimeout,
    };
    console.log("ca;;;;;;;llll....",config)
    API.call(config, callback);
  },

  /*
    * This method will take URL path as string for "routeurl"
    * params will take body: {key: value}, note: not stringify
    * Header:  {key: value}, note: not stringify
    * callback will give us two params (error, responseJson),
      so check for error not equal to null then process json
      else throw alert in your UI by using error parameter.
  */

  call: (config, callback) => {
    axios(config)
      .then((response) => {

        callback(null, response);
      })
      .catch((error) => {
        callback(error, null);
        errorScreen(error);

        throw error;
      });
  },

}