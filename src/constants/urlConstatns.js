//import { useSelector } from 'react-redux';
export const ROOT_URL = 'https://vjd.webschedio.in/backend/';
//export const ROOT_URL = 'https://vjdnew-385705.uc.r.appspot.com/';
//export const ROOT_URL = 'https://localhost:8080/';
export const API_URL = {
targetscore:`${ROOT_URL}interruption/getexcelldata`,
 register:`${ROOT_URL}users/register`  ,
 login:`${ROOT_URL}users/login`  ,
 targetscoreTwenty:`${ROOT_URL}interruptionT20/getexcelldataT20`,
 pdfView:`${ROOT_URL}pdf/50overspdf`,
 pdfViewTwenty:`${ROOT_URL}pdf/T20overspdf`,
 viewprofile:`${ROOT_URL}users/viewprofile`,
 editprofile:`${ROOT_URL}users/editprofile`,
//  getProuser:`${ROOT_URL}payments/getProuser`,
 getAllSubscriptions:`${ROOT_URL}payments/getAllSubscriptions`,
 logout:`${ROOT_URL}users/logout`,
    }
    