import HomeScreen from '../views/screens/HomeScreen';
import LoginScreen from '../views/screens/LoginScreen';
import FirstInnings from '../views/screens/OneDayer/FirstInnings';
import Loader from '../views/core-libs/Loader/Loader';
import SecondInnings from '../views/screens/OneDayer/SecondInnings';
import ResultPage from '../views/screens/OneDayer/ResultPage';
import FirstInningsTwenty from '../views/screens/OneDayer/FirstInningsTwenty';
import SecondInningsTwenty from '../views/screens/OneDayer/SecondInningsTwenty';
import ResultPageTwenty from '../views/screens/OneDayer/ResultPageTwenty';
import Login from '../views/screens/Login';
import pdfView from '../views/screens/OneDayer/pdfView';
// import ProfilePage from '../views/screens/ProfilePage';
// import ProfileEditScreen from '../views/screens/OneDayer/ProfileEditScreen';

const appRoutes = [
{name:'HomeScreen',component:HomeScreen},
{name: 'LoginScreen',component: LoginScreen },
{name: 'Login',component: Login},
{name:'FirstInnings',component:FirstInnings},
{name:'SecondInnings',component:SecondInnings},
{name:'SecondInningsTwenty',component:SecondInningsTwenty},
{name:'ResultPage',component:ResultPage},
{name:'ResultPageTwenty',component:ResultPageTwenty},
{name:'FirstInningsTwenty',component:FirstInningsTwenty},
{name:'pdfView',component:pdfView},
// {name:'ProfileEditScreen',component:ProfileEditScreen},
// {name:'PaymentReport',component:PaymentReport},
// {name:'SubscriptionReport',component:SubscriptionReport},
{name:'Loader',component:Loader},
 
    ];
    // const drawerRoutes = [

       
    
    //     // {name:'ProfilePage',component:ProfilePage},
       
    //         ];

                
export  {appRoutes}; 
