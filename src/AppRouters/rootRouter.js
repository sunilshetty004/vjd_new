import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AppRoutes from './AppRoute';
import DrawerRouter from './DrawerRoutes';



const RootRouter = () =>{

const AppStack = createStackNavigator();
// const Drawer = createDrawerNavigator();
// const userData = useSelector(state => state.UserReducer?.userData);

 return( 
   <NavigationContainer> 
  
   <AppRoutes/>      
    
    {/* // <DrawerRouter/>   */}
   </NavigationContainer>
   
 )
}
export default RootRouter;
