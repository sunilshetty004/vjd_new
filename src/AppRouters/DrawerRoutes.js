import React from 'react';
import {
    createDrawerNavigator, DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
    
} from '@react-navigation/drawer';
import {
  ToastAndroid,
    TouchableOpacity
  } from 'react-native';
import { Text, ArrowBackIcon, Image,View, HStack,useToast} from 'native-base';
import AppRoute from './AppRoute'
import { drawerRoutes, drawerRoutesWithoutRegister } from '../constants/routes';
// import { NavConstants } from '../constnts/navConstants';
import { useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import {logOutRequest} from '../../src/store/actions/userActions';


function CustomDrawerContent(props) {
  const [isshow,show] = React.useState(false);
  const [isshowExchange,showExchange] = React.useState(false);
  const [isshowHistory,showHistory] = React.useState(false);
  const toast = useToast();
  console.log("drawer propsssss", props);
    const {navigation} = props;
    const userData = useSelector(state => state.UserReducer?.userDataLogin);

    // console.log("userdata",userData.userId)
    const dispatch = useDispatch();
  
      
       const onPressLogout = () => {
        console.log("userdata###########",userData.userId)
        var data ={
          userId:userData.userId
        }
        dispatch(logOutRequest(data,(error,success)=>{
         
            if(error){
         toast.show({
                title: "Logout Failed",
                status: "error",})
            }
            //console.log("...........gan.......",success)
            if(success){
              console.log("success logout drawer layout", success)
             // dispatch(logOutRequest());
              // ToastAndroid.showWithGravity(
              //   'Logout Successfully',
              //   ToastAndroid.LONG,
              //   ToastAndroid.BOTTOM
              // ) 
                 props.navigation.navigate('Login'); 
              console.log("logout success",success);
            }
            
            // if (success.data.status == 200){
            //   console.log("status success",success.data.status);
            //   props.navigation.navigate(NavConstants.home);
            //   toast.show({
            //     title: "Login Successfully",
            //     status: "success",
            //   })
            // } 
            // if (success.data.status == 404){
            //   console.log("status success",success.data.status);
            //   toast.show({
            //     title: "Invalid credentials",
            //     status: "warning",
            //     description: "Please enter a valid data",
            //   })
            // }  
          }))
         
         // console.log("remove item from carttttts", item);
        //}
       
         
           }
  //  console.log("userId initial", userData);
    return (
    //    (userData != null ?  
        <DrawerContentScrollView {...props}>
             <TouchableOpacity
     onPress={() => {props.navigation.goBack(null) }}
       >
             <ArrowBackIcon mt={5} size='lg' mb={5} ml={2}/>
             </TouchableOpacity>

          
      



        
        






    <DrawerItemList {...props} activeTintColor='#2196f3' activeBackgroundColor='rgba(0, 0, 0, .04)' inactiveTintColor='rgba(0, 0, 0, .87)' inactiveBackgroundColor='transparent' style={{backgroundColor: '#000000'}} labelStyle={{color: '#000'}} />
            <DrawerItem label={() => <Text color= 'white' justifyContent='center' alignSelf='center' bold fontSize={20}>Logout</Text>}
        style={{backgroundColor: '#008081'}} 
        onPress={() =>onPressLogout()} 
        // onPress={()=>{show(true)}}
            />
         
             

        </DrawerContentScrollView>

);
}

const DrawerRouter = () => {

    const Drawer = createDrawerNavigator();

    return (
        // (userData != null ? 
        <Drawer.Navigator initialRouteName="Home" screenOptions={{headerShown:false}} drawerContent={props => <CustomDrawerContent {...props} />}>
           <Drawer.Screen name="Home" component={AppRoute} 
        options={{
            drawerIcon: config => <Image source = {require('@Asset/home.png')} size={25} 
             alt="img"/>
        }} />
                
            {drawerRoutes.map((item,idx) => <Drawer.Screen name={item.name} component={item.component} key={idx}
             options={{
                drawerIcon: () => (
                  item.icon== 'LoginScreen' ? <Image source = {require('@Asset/home.png')}  alt="img" size={25}/>: 
                  item.icon== 'LoginScreen' ? <Image source = {require('@Asset/home.png')}  alt="img" size={25}/> :
                  item.icon== 'LoginScreen' ? <Image source = {require('@Asset/home.png')}  alt="img" size={25}/> :
                  <Image source = {require('@Asset/home.png')}  alt="img" size={25}/>
              ),
            }}
            />)}
        
        </Drawer.Navigator> 
        //: 
        // <Drawer.Navigator initialRouteName="DashBoardScreen"  drawerContent={props => <CustomDrawerContent {...props} />}>
        //    <Drawer.Screen name="Home" component={AppRoute} 
        // // options={{
        // //     drawerIcon: config => <Image source = {require('@Asset/home.png')} 
        // //                alt="img"/>
        // // }} 
        // />
                
        //     {drawerRoutesWithoutRegister.map((item,idx) => <Drawer.Screen name={item.name} component={item.component} key={idx}
        //     //  options={{
        //     //     drawerIcon: () => (
        //     //       item.icon== NavConstants.faqScreen ? <Image source = {require('@Asset/faq.png')}  alt="img"/> :
        //     //       item.icon== NavConstants.articlesScreen ? <Image source = {require('@Asset/newspaper.png')} alt="img" />:
                 
        //     //      null
           
        //     //     ),
        //     //   }}
        //       />
        //       )}
        //        {/* <Drawer.Screen name={NavConstants.faqScreen} component={FaqScreen} 
        //      options={{
        //         drawerIcon: () => (
        //           <Image source = {require('@Asset/faq.png')}  alt=""/>
        //         //  item.icon== NavConstants.articlesScreen ? <Image source = {require('@Asset/newspaper.png')} alt="" />:
                 
        //         // null
           
        //         ),
        //       }}/>
        //        <Drawer.Screen name={NavConstants.articlesScreen} component={ArticlesScreen} 
        //      options={{
        //         drawerIcon: () => (
        //         <Image source = {require('@Asset/newspaper.png')} alt="" />
                 
            
           
        //         ),
        //       }}/> */}
              
        // </Drawer.Navigator>)
    )
}
export default DrawerRouter;
