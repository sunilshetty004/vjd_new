import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {appRoutes} from '../constants/routes'
// import DashBoardScreen from '../views/screens/DashBoardScreen';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import LoginScreen from '../views/screens/LoginScreen';
import HomeScreen from '../views/screens/HomeScreen';
const Stack = createStackNavigator();


const AppNavigators = () => {
  const userData = useSelector(state => state.UserReducer?.token);
 console.log("Token",userData)
  return(
    
  <Stack.Navigator    screenOptions={{
    headerShown: false
  }}
  initial={"Login"} initialRouteName=
  {(userData == null) ? 
  "Login" 
  : "HomeScreen" }
  >
    
{/* <Stack.Navigator    screenOptions={{
    headerShown: false
  }}
  initial={"HomeScreen"} initialRouteName=
  {(userData == null) ? 
  "HomeScreen" 
  : "HomeScreen" }
  > */}
    
  
    
    {
      appRoutes.map((item,idx)=> <Stack.Screen name={item.name} component={item.component} key={idx}/>)
    }
  </Stack.Navigator>)
  


};
export default AppNavigators;