import * as types from '../../constants/actionTypeConstants';
 
const isLoading = (isLoading) =>{
    return {
        type:types.LOADER,
        payload:isLoading
    }
}


 const hideLoader = () => {
    return {
      type: types.HIDE_LOADER
    };
  };
export {isLoading,hideLoader}