import * as types from '../../constants/actionTypeConstants';
import { mpinloginWatcher } from '../../sagas/userSaga';

//register
const signInRequest = (data, callback) => {
  return {
    type: types.SIGN_IN_REQUEST, payload: data, callback
  }
}

const signInSuccess = (data) => {
  return {
    type: types.SIGN_IN_SUCCESS, payload: data
  }
}
const signInFailure = (error) => {
  return {
    type: types.SIGN_IN_FAILURE, payload: error
  }
}



//login
const loginRequest = (data,callback) => {
  console.log("data",data);
  console.log("callback",callback);

  return {
    type: types.LOG_IN_REQUEST, payload: data, callback,  }
}

const loginSuccess = (data) => {
  return {
    type: types.LOG_IN_SUCCESS, payload: data,
  }
}
const loginFailure = (error) => {
  return {
    type: types.LOG_IN_FAILURE, payload: error,
  }
}


//TArget Score
const targetRequest = (data, callback) => {
  return {
    type: types.TARGET_REQUEST, payload: data, callback
  }
}

const targetSuccess = (data) => {
  return {
    type: types.TARGET_SUCCESS, payload: data
  }
}
const targetFailure = (error) => {
  return {
    type: types.TARGET_FAILURE, payload: error
  }
}


//TArget Score TWenty
const targetRequestTwenty= (data, callback) => {
  return {
    type: types.TARGET_REQUEST_TWENTY, payload: data, callback
  }
}

const targetSuccessTwenty = (data) => {
  return {
    type: types.TARGET_SUCCESS_TWENTY, payload: data
  }
}
const targetFailureTwenty = (error) => {
  return {
    type: types.TARGET_FAILURE_TWENTY, payload: error
  }
}



//PDF View
const pdfViewRequest= (data, callback) => {
  return {
    type: types.PDF_VIEW_REQUEST, payload: data, callback
  }
}

const pdfViewSuccess = (data) => {
  return {
    type: types.PDF_VIEW_SUCCESS, payload: data
  }
}
const pdfViewFailure = (error) => {
  return {
    type: types.PDF_VIEW_FAILURE, payload: error
  }
}




//PDF ViewTWenty
const pdfViewTwentyRequest= (data, callback) => {
  return {
    type: types.PDF_VIEW_TWENTY_REQUEST, payload: data, callback
  }
}

const pdfViewTwentySuccess = (data) => {
  return {
    type: types.PDF_VIEW_TWENTY_SUCCESS, payload: data
  }
}
const pdfViewTwentyFailure = (error) => {
  return {
    type: types.PDF_VIEW_TWENTY_FAILURE, payload: error
  }
}



//subscribe
const subscribeRequest= (data, callback) => {
  return {
    type: types.SUBSCRIBE_REQUEST, payload: data, callback
  }
}

const subscribeSuccess = (data) => {
  return {
    type: types.SUBSCRIBE_SUCCESS, payload: data
  }
}
const subscribeFailure = (error) => {
  return {
    type: types.SUBSCRIBE_FAILURE, payload: error
  }
}


//subscription verification
const verifySubscribeRequest= (data, callback) => {
  return {
    type: types.VERIFY_SUBSCRIBE_REQUEST, payload: data, callback
  }
}

const verifySubscribeSuccess = (data) => {
  return {
    type: types.VERIFY_SUBSCRIBE_SUCCESS, payload: data
  }
}
const verifySubscribeFailure = (error) => {
  return {
    type: types.VERIFY_SUBSCRIBE_FAILURE, payload: error
  }
}

//get all subscriptions
const getAllSubscriptionsRequest= ( data,callback) => {
  return {
    type: types.GET_ALL_SUBSCRIPTIONS_REQUEST,  payload: data, callback
  }
}

const getAllSubscriptionsSuccess = (data) => {
  return {
    type: types.GET_ALL_SUBSCRIPTIONS_SUCCESS, payload: data
  }
}
const getAllSubscriptionsFailure = (error) => {
  return {
    type: types.GET_ALL_SUBSCRIPTIONS_FAILURE, payload: error
  }
}




//view profile
const viewProfileRequest= ( callback) => {
  return {
    type: types.VIEW_PROFILE_REQUEST,  callback
  }
}

const viewProfileSuccess = (data) => {
  return {
    type: types.VIEW_PROFILE_SUCCESS, payload: data
  }
}
const viewProfileFailure = (error) => {
  return {
    type: types.VIEW_PROFILE_FAILURE, payload: error
  }
}

//edit profile
const editProfileRequest= (data, callback) => {
  return {
    type: types.EDIT_PROFILE_REQUEST, payload: data, callback
  }
}

const editProfileSuccess = (data) => {
  return {
    type: types.EDIT_PROFILE_SUCCESS, payload: data
  }
}
const editProfileFailure = (error) => {
  return {
    type: types.EDIT_PROFILE_FAILURE, payload: error
  }
}


//getPaymentReportRequest getPaymentReportSuccess getPaymentReportFailure
//get all payment
const getPaymentReportRequest= (callback) => {
  return {
    type: types.GET_PAYMENT_REPORT_REQUEST, callback
  }
}

const getPaymentReportSuccess = (data) => {
  return {
    type: types.GET_PAYMENT_REPORT_SUCCESS, payload: data
  }
}
const getPaymentReportFailure = (error) => {
  return {
    type: types.GET_PAYMENT_REPORT_FAILURE, payload: error
  }
}


//GET_SUBSCRIPTION_REPORT
const getSubscriptionReportRequest= (callback) => {
  return {
    type: types.GET_SUBSCRIPTION_REPORT_REQUEST, callback
  }
}

const getSubscriptionReportSuccess = (data) => {
  return {
    type: types.GET_SUBSCRIPTION_REPORT_SUCCESS, payload: data
  }
}
const getSubscriptionReportFailure = (error) => {
  return {
    type: types.GET_SUBSCRIPTION_REPORT_FAILURE, payload: error
  }
}


//getProUsertRequest
const getProUsertRequest= (callback) => {
  return {
    type: types.GET_PROUSER_REQUEST, callback
  }
}

const getProUserSuccess = (data) => {
  return {
    type: types.GET_PROUSER_SUCCESS, payload: data
  }
}
const getProUserFailure = (error) => {
  return {
    type: types.GET_PROUSER_FAILURE, payload: error
 
  }
}
//unAuthorizedrequest
 const navigateToLogin = () => {
  return {
    type: 'NAVIGATE_TO_LOGIN',
  };
};


const unAuthorizedRequest = (callback) => {
  return {
    type: types.UNAUTHORIZED_REQUEST,callback
  }
}

const unAuthorizedSuccess = (data) => {
  return {
    type: types.UNAUTHORIZED_SUCCESS, payload:data
  }
}

const unAuthorizedFailure = (error) => {
  return {
    type: types.UNAUTHORIZED_FAILURE,payload:error
  }
}

//logout

const logOutRequest = (data,callback) => {
  return {
    type: types.LOG_OUT_REQUEST,payload:data,callback
  }
}

const logOutSuccess = (data) => {
  return {
    type: types.LOG_OUT_SUCCESS, payload:data
  }
}

const logOutFailure = (error) => {
  return {
    type: types.LOG_OUT_FAILURE,payload:error
  }
}




export {
 
 
 
 
  signInRequest,signInSuccess,signInFailure,
  loginSuccess,loginRequest,loginFailure,
 logOutRequest,logOutSuccess,logOutFailure,
 targetRequest,targetFailure,targetSuccess,
 targetFailureTwenty,targetRequestTwenty,targetSuccessTwenty,
 
 pdfViewFailure,pdfViewRequest,pdfViewSuccess,
 pdfViewTwentyFailure,pdfViewTwentySuccess,pdfViewTwentyRequest,
 subscribeFailure,subscribeRequest,subscribeSuccess,
 verifySubscribeFailure,verifySubscribeSuccess,verifySubscribeRequest,
 getAllSubscriptionsRequest,getAllSubscriptionsSuccess,getAllSubscriptionsFailure,
 viewProfileFailure,viewProfileRequest,viewProfileSuccess,
 editProfileFailure,editProfileRequest,editProfileSuccess,
 getPaymentReportFailure,getPaymentReportRequest,getPaymentReportSuccess,
 getSubscriptionReportRequest,getSubscriptionReportSuccess,getSubscriptionReportFailure,
 getProUserFailure,getProUserSuccess,getProUsertRequest,
 unAuthorizedRequest,unAuthorizedSuccess,unAuthorizedFailure,navigateToLogin
}