import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";
import { persistStore, persistReducer, persistCombineReducers } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import storages from "redux-persist/lib/storage";


import rootReducer from "../../src/store/reduces";
import rootSaga from '../../src/sagas';
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
   // whitelist: [ 'auth' ,'home'],
    //timeout: null
  };
  
  
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// Mount it on the Store
// const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware, logger));
// store.subscribe(() => {
//   //this is just a function that saves state to localStorage
//   saveState(store.getState());
// }); 

// Run the saga
//sagaMiddleware.run(rootSaga);
// let persistor = persistStore(store);

// const saveState = (state) => {
//   try {
//     const serializedState = JSON.stringify(state);
//     localStorage.setItem('state', serializedState);
//   } catch (e) {
//     // Ignore write errors;
//   }
// };
// store.subscribe(() => {
//   saveState(store.getState());
// });

//export { store, persistor };
export default () => {
  //const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));

  const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware, logger));
  let persistor = persistStore(store);


  // then run the saga
  sagaMiddleware.run(rootSaga);

  return { store, persistor };
};
