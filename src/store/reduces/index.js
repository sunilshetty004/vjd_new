import { combineReducers } from "redux";

import UserReducer from "./userReducer";
import UtilsReducer from './utilReducer';


const rootReducers = combineReducers({
    UserReducer: UserReducer,
     UtilsReducer:UtilsReducer,
  
});

export default rootReducers;
