import * as types from '../../constants/actionTypeConstants';
const initialState = {
  userData: null,
  userDataLogin:null,
  token:null,
  tokenExpiration:null,
  target:{},
  targetTwenty:{},
  pdf:{},
  pdfTwenty:{},
  subscribe:{},
  onVerifySubscribe:{},
  subscriptionList:{},
  viewProfile:{},
  editProfile:{},
  paymentReport:{},
  subscriptionReport:{},
  proUserList:{},
  route:{},
}


export default function (state = { ...initialState }, action) {
  switch (action.type) {

//register
case types.SIGN_IN_REQUEST:
  return Object.assign({}, state, {
    status: "REQUESTING",
  });
case types.SIGN_IN_SUCCESS: 
return Object.assign({}, state, {
status: "SUCCESS",
...state,
userData: action.payload,
// token: action.payload.token,

});
case types.SIGN_IN_FAILURE:
return Object.assign({}, state, {
status: "ERROR",
error: action.error,
});

// console.log("token",token)




    //login
    case types.LOG_IN_REQUEST:
          return Object.assign({}, state, {
            status: "REQUESTING",
          });
    case types.LOG_IN_SUCCESS: 
    return Object.assign({}, state, {
      status: "SUCCESS",
      ...state,
      userDataLogin: action.payload,
     token: action.payload.token,
     tokenExpiration:action.payload.expirationDate

    });
    case types.LOG_IN_FAILURE:
      return Object.assign({}, state, {
        status: "ERROR",
        error: action.error,
      });

// console.log("token",token)


    //Target
    case types.TARGET_REQUEST:
      return Object.assign({}, state, {
        status: "REQUESTING",
      });
    case types.TARGET_SUCCESS: return {
      ...state,
      target: action.payload
    }

    case types.TARGET_FAILURE:
      return Object.assign({}, state, {
        status: "ERROR",
        error: action.error,
      });




       //TargetTwenty
    case types.TARGET_REQUEST_TWENTY:
      return Object.assign({}, state, {
        status: "REQUESTING",
      });
    case types.TARGET_SUCCESS_TWENTY: return {
      ...state,
      targetTwenty: action.payload
    }

    case types.TARGET_FAILURE_TWENTY:
      return Object.assign({}, state, {
        status: "ERROR",
        error: action.error,
      });

//PDF VIEW
case types.PDF_VIEW_REQUEST:
  return Object.assign({}, state, {
    status: "REQUESTING",
  });
case types.PDF_VIEW_SUCCESS:
   return {
  ...state,
  pdf: action.payload.result
}

case types.PDF_VIEW_FAILURE:
  return Object.assign({}, state, {
    status: "ERROR",
    error: action.error,
  });




  //PDF VIEW TWENTY
case types.PDF_VIEW_TWENTY_REQUEST:
  return Object.assign({}, state, {
    status: "REQUESTING",
  });
case types.PDF_VIEW_TWENTY_SUCCESS:
   return {
  ...state,
  pdfTwenty: action.payload.result
}

case types.PDF_VIEW_TWENTY_FAILURE:
  return Object.assign({}, state, {
    status: "ERROR",
    error: action.error,
  });



  //subscription api
  
  case types.SUBSCRIBE_REQUEST:
    return Object.assign({}, state, {
      status: "REQUESTING",
    });
  case types.SUBSCRIBE_SUCCESS: return {
    ...state,
    subscribe: action.payload
  }

  case types.SUBSCRIBE_FAILURE:
    return Object.assign({}, state, {
      status: "ERROR",
      error: action.error,
    });

 //verification of  subscription 
  
 case types.VERIFY_SUBSCRIBE_REQUEST:
  return Object.assign({}, state, {
    status: "REQUESTING",
  });
case types.VERIFY_SUBSCRIBE_SUCCESS: return {
  ...state,
  onVerifySubscribe: action.payload
}

case types.VERIFY_SUBSCRIBE_FAILURE:
  return Object.assign({}, state, {
    status: "ERROR",
    error: action.error,
  });

//get all subscriptions for view
case types.GET_ALL_SUBSCRIPTIONS_REQUEST:
        return Object.assign({}, state, {
          status: "REQUESTING",
        }); 
      case types.GET_ALL_SUBSCRIPTIONS_SUCCESS: return {
        ...state,
        subscriptionList: action.payload
      }
      case types.GET_ALL_SUBSCRIPTIONS_FAILURE:
        return Object.assign({}, state, {
          status: "ERROR",
          error: action.error,
        });

//view profile
case types.VIEW_PROFILE_REQUEST:
        return Object.assign({}, state, {
          status: "REQUESTING",
        }); 
      case types.VIEW_PROFILE_SUCCESS: return {
        ...state,
        viewProfile: action.payload
      }
      case types.VERIFY_SUBSCRIBE_FAILURE:
        return Object.assign({}, state, {
          status: "ERROR",
          error: action.error,
        });

   //EDIT PROFILE
   case types.EDIT_PROFILE_REQUEST:
    return Object.assign({}, state, {
      status: "REQUESTING",
    });
  case types.EDIT_PROFILE_SUCCESS: return {
    ...state,
    editProfile: action.payload
  }

  case types.EDIT_PROFILE_FAILURE:
    return Object.assign({}, state, {
      status: "ERROR",
      error: action.error,
    });


//payment report
case types.GET_PAYMENT_REPORT_REQUEST:
        return Object.assign({}, state, {
          status: "REQUESTING",
        }); 
      case types.GET_PAYMENT_REPORT_SUCCESS: return {
        ...state,
        paymentReport: action.payload
      }
      case types.GET_PAYMENT_REPORT_FAILURE:
        return Object.assign({}, state, {
          status: "ERROR",
          error: action.error,
        });





        //GET_SUBSCRIPTION_RE
        case types.GET_SUBSCRIPTION_REPORT_REQUEST:
          return Object.assign({}, state, {
            status: "REQUESTING",
          }); 
        case types.GET_SUBSCRIPTION_REPORT_SUCCESS: return {
          ...state,
         subscriptionReport: action.payload
        }
        case types.GET_SUBSCRIPTION_REPORT_FAILURE:
          return Object.assign({}, state, {
            status: "ERROR",
            error: action.error,
          });
  

           //GET PRO USER
        case types.GET_PROUSER_REQUEST:
          return Object.assign({}, state, {
            status: "REQUESTING",
          }); 
        case types.GET_PROUSER_SUCCESS: return {
          ...state,
         proUserList: action.payload
        }
        case types.GET_PROUSER_FAILURE:
          return Object.assign({}, state, {
            status: "ERROR",
            error: action.error,
          });



          //unauthorized 
          case types.UNAUTHORIZED_REQUEST:
            return Object.assign({}, state, {
              status: "REQUESTING",
            });
    
            case types.UNAUTHORIZED_SUCCESS: return {
              ...initialState,
              
            }


            case types.UNAUTHORIZED_FAILURE:
              return Object.assign({}, state, {
                status: "ERROR",
                error: action.error,
              });


//login
case 'NAVIGATE_TO_LOGIN':
      return { ...initialState, route: 'Login' };


      //logout
      case types.LOG_OUT_REQUEST:
        return Object.assign({}, state, {
          status: "REQUESTING",
        });

        case types.LOG_OUT_SUCCESS: return {
          ...initialState,
          
        }
        case types.LOG_OUT_FAILURE:
    return Object.assign({}, state, {
      status: "ERROR",
      error: action.error,
    });

      default: return state
    }
  }