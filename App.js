
import React,{useEffect} from 'react';
import {
  Text, 
  View,
} from 'react-native';
import  RootRouter from './src/AppRouters/rootRouter'
import {SafeAreaProvider} from 'react-native-safe-area-context';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Loader from "./src/views/core-libs/Loader/Loader";
import configureStore from "./src/store";
import { fontFamily } from 'styled-system';
import { NativeBaseProvider, extendTheme } from 'native-base';

export const { store, persistor } = configureStore();
const App = () => {
  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1020); 
    // amount of time the splash is shown from the time component is rendered
  }, []);
    


  return (
    <SafeAreaProvider>
    <NativeBaseProvider >
<Provider store={store}>
    <PersistGate loading={null} persistor={persistor}> 




       <Loader color="blue.500" /> 
       <RootRouter/>  
</PersistGate>
     </Provider>
    </NativeBaseProvider>
     </SafeAreaProvider>
     
  );
};


export default App;
